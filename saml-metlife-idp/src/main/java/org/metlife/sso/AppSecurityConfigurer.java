package org.metlife.sso;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.AntPathRequestMatcher;
import org.springframework.stereotype.Component;

@SuppressWarnings("deprecation")
@Component
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class AppSecurityConfigurer extends WebSecurityConfigurerAdapter {

	@Autowired
	private SecurityProperties security;

	@Autowired
	private DataSource dataSource;

	@SuppressWarnings("deprecation")
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/saml/metadata.xml")
				.permitAll()
				.antMatchers("/css/**")
				.permitAll().anyRequest()
				.fullyAuthenticated().and().formLogin().loginPage("/login")
				.failureUrl("/login?error")
				.permitAll()
				.and()
				.logout()
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
				.permitAll();
	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication().dataSource(this.dataSource);
	}

}