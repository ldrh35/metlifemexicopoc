package org.metlife.sso;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

	@RequestMapping("/")
		public void home(HttpServletResponse httpServletResponse) throws IOException {
		    httpServletResponse.sendRedirect("https://google.com.mx");
		}

}
