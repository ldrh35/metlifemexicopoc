/***************************************************************************

 **************************************************************************/
package org.metlife.sso;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.common.io.Files;

@Controller
public class MetadataController {
    
    @RequestMapping(value = "/saml/metadata.xml", method = RequestMethod.GET)
    public void getFile(HttpServletResponse response) throws IOException {
        File file = new File("metadata.xml");
        Files.copy(file, response.getOutputStream());
        response.flushBuffer();
    }

}
