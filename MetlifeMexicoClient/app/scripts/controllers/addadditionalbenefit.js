'use strict';

/**
 * @ngdoc function
 * @name metLifeApp.controller:AddAdditionalBenefitCtrl
 * @description
 * # AddAdditionalBenefitCtrl
 * Controller of the metLifeApp
 */
angular.module('metLifeApp')
    .controller('AddAdditionalBenefitCtrl', ['$scope', '$uibModalInstance', 'plans', 'webServices', function ($scope, $uibModalInstance, plans, webServices) {
        $scope.options = [{value:''}];
        $scope.plans = plans;
        $scope.benefitTypes = ['Radio','Input','List'];
        
        $scope.addOption = function(e){
            e.preventDefault();
            for(var i = 0, len = $scope.options.length; i<len; i++){
                if($scope.options[i].value=== ''){
                    return false;
                }
            }
            $scope.options.push({value:''});
        };
        
        $scope.removeOption = function(e, index){
            e.preventDefault();
            if($scope.options.length>1){
                $scope.options.splice(index, 1);
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.createAdditionalBenefit = function () {
            $uibModalInstance.close($scope.benefit);
        };
  }]);
