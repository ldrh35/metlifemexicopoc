'use strict';

/**
 * @ngdoc function
 * @name metLifeApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the metLifeApp
 */
angular.module('metLifeApp')
    .controller('MainFormCtrl', ['$rootScope', '$timeout', '$scope', '$location', '$filter', '$uibModal', 'applicationInfo', 'webServices', 'applicationForm', function ($rootScope, $timeout, $scope, $location, $filter, $uibModal, applicationInfo, webServices, applicationForm) {
        $scope.section = 0;
        $scope.editing = false;
        $scope.showingPersonalInformation = false;
        $scope.applicationForm = applicationForm;
        $scope.section = 1;
        console.log('applicationForm', applicationForm);
        console.log('applicationInfo', applicationInfo);

        $scope.getApplicationFormValues = function (id) {
            if ($scope.applicationForm) {
                return $scope.applicationForm[id];
            } else {
                return [];
            }
        };

        if (applicationInfo) {
            $scope.neighborhoods = [applicationInfo.neighborhood];
            $scope.application = applicationInfo;
            $scope.application.emailConfirm = $scope.application.email;
            $scope.selectedNeighborhood = $scope.application.neighborhood;

            $scope.application.occupation = {
                occupationId: $scope.application.occupationId,
                ocupationAlias: $scope.application.ocupationAlias,
                ocupationCode: $scope.application.ocupationCode,
                ocupationName: $scope.application.ocupationName
            };

            var input = $scope.application.birthDate;
            var parts = input.split('-');
            $scope.application.birthDate = new Date(parts[0], parts[1] - 1, parts[2]); // Note: months are 0-basednew Date();

            var plans = $scope.getApplicationFormValues('plans');
            $scope.application.plan = $filter('filter')(plans, {
                planId: parseInt($scope.application.plan)
            })[0];

            try {
                $scope.application.additionalBenefitsJsonKv = JSON.parse($scope.application.additionalBenefitsJsonKv.replace(/\\"/g, '"'));
            } catch (e) {
                console.log(e);
            }

            if (!$scope.application.billingInformations || $scope.application.billingInformations.length < 1) {
                $scope.paymentDeductions = [{
                    paymentType: 'Descuento por nomina' // default value
                }];
            } else {
                $scope.paymentDeductions = angular.copy($scope.application.billingInformations);
            }
            $scope.editing = true;
            $scope.planEditing = true;
        } else {
            $scope.application = {};
            $scope.showingPersonalInformation = true;
            $scope.paymentDeductions = [{
                paymentType: 'Descuento por nomina' // default value
                }];
        }
        $scope.datePickerOptions = {
            datepickerMode: 'year'
        };

        $scope.additionalBenefits = [];

        $scope.basicBenefitOptions = ['1,000,000', '900,000', '800,000', '500,000'];

        $scope.openAdditionalBenefitModal = function (e) {
            e.preventDefault();
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/addadditionalbenefit.html',
                size: 'lg',
                controller: 'AddAdditionalBenefitCtrl',
                resolve: {
                    plans: function () {
                        return $scope.getApplicationFormValues('plans');
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                console.log(selectedItem)
            }, function () {
                console.log('modal-component dismissed at: ' + new Date());
            });
        };

        $scope.showPersonalInformation = function (e) {
            e.preventDefault();
            /*if ($scope.application.applicationId) {
                if (!$scope.showingPersonalInformation) {
                    $rootScope.$emit('loadingOn');
                    webServices.showPersonalInformation({
                        cutomerId: $scope.application.customerId,
                        applicationId: $scope.application.applicationId
                    }, function (resp) {
                        $rootScope.$emit('loadingOff');
                        console.log(resp);
                        $scope.showingPersonalInformation = !$scope.showingPersonalInformation;
                    }, function () {
                        $rootScope.$on('loadingOff');
                    });
                }

            }*/
            $scope.showingPersonalInformation = !$scope.showingPersonalInformation;
        };

        $scope.getOccupations = function (query) {
            return webServices.getOccupations({
                query: query
            });
        };

        $scope.getEmployers = function (query) {
            return webServices.getEmployers({
                query: query
            });
        };

        $scope.changeSection = function (e, section) {
            e.preventDefault();
            $scope.section = section;
        };

        $scope.updateZipCodeInfo = function (e) {
            if (typeof $scope.application.zipCode !== 'undefined' && $scope.application.zipCode.length === 5) {
                webServices.getZipInformation($scope.application.zipCode, function (resp) {
                    resp.colonias.push('-- Otra --');
                    $scope.neighborhoods = resp.colonias;
                    $scope.application.municipality = resp.municipio;
                    $scope.application.city = resp.municipio;
                    $scope.application.state = resp.estado;
                });
            }
        };

        $scope.saveNeighborhood = function () {
            if ($scope.selectedNeighborhood !== '-- Otra --') {
                $scope.application.neighborhood = $scope.selectedNeighborhood;
            }
        };

        $scope.changePlan = function () {
            if ($scope.planEditing) {
                $scope.planEditing = false;
            } else {
                delete $scope.application.additionalBenefits;
            }
            var addBenefits = $scope.getApplicationFormValues('additionalBenefits');
            if (Array.isArray(addBenefits)) {
                $scope.application.additionalBenefitsJsonKv = addBenefits.filter(function (benefit) {
                    for (var i = 0, len = benefit.plan.length; i < len; i++) {
                        if ($scope.application.plan && benefit.plan[i] === $scope.application.plan.planId) {
                            return true;
                        }
                    }
                    return false;
                });
            } else {
                $scope.application.additionalBenefitsJsonKv = [];
            }
        };

        $scope.makeJSON = function (str) {
            try {
                str += '';
                str = '[' + str.replace(/[{}]/g, '').replace(/[[\]]/g, '') + ']';
                return JSON.parse(str);
            } catch (err) {
                return [];
            }
        }

        $scope.calculateBasicPremium = function (value) {
            value += '';
            value = value.replace(/,/g, "");
            value = parseFloat(value, 10) * 0.002;
            return $filter('currency')(value, '$', 2);
        };

        $scope.calculatePremium = function (benefit) {
            if (!benefit.checked) {
                benefit.benefitPremium = 0;
                return '';
            }
            var value = benefit.benefitAmmount + '';
            value = value.replace(/,/g, "");
            if (isNaN(benefit.formula)) {
                value = parseFloat(value, 10) * 0.002;
            } else {
                value = parseFloat(value, 10) * benefit.formula;
            }
            if (!isNaN(value)) {
                benefit.benefitPremium = value;
            }
            return $filter('currency')(value, '$', 2);
        };

        $scope.calculateTotalPremium = function () {
            var total = 0,
                value;
            if ($scope.application.basicBenefit) {
                value = $scope.application.basicBenefit + '';
                value = value.replace(/,/g, "");
                value = parseFloat(value, 10) * 0.002;
                total += value;
            }
            if ($scope.application.additionalBenefitsJsonKv) {
                for (var i = 0, len = $scope.application.additionalBenefitsJsonKv.length; i < len; i++) {
                    if ($scope.application.additionalBenefitsJsonKv[i].benefitPremium) {
                        if ($scope.application.additionalBenefitsJsonKv[i].checked && !isNaN($scope.application.additionalBenefitsJsonKv[i].benefitPremium)) {
                            total += $scope.application.additionalBenefitsJsonKv[i].benefitPremium;
                        }

                    }
                }
            }
            $scope.application.amount = total;
            return $filter('currency')(total, '$', 2);
        };

        $scope.addPaymentDeduction = function (e) {
            console.log($scope.paymentType);
            e.preventDefault();
            if ($scope.paymentType) {
                $scope.paymentDeductions.push({
                    paymentType: $scope.paymentType
                });
                $scope.paymentType = null;
            } else {
                $scope.$broadcast('invalidField', 'paymentType');
            }
        };

        $scope.removePaymentDeduction = function (e, index) {
            e.preventDefault();
            $scope.paymentDeductions.splice(index, 1);
        };

        $scope.validateForm = function () {
            $scope.section = 1;
            $timeout(function () {
                $scope.$broadcast('invalidField', 'application.name');
            });
        };

        $scope.test = function () {
            console.log("Test", $scope.application, $scope.applicationHTMLForm, $scope.paymentDeductions);
        };

        $scope.cancelApplication = function (e) {
            e.preventDefault();
            $location.path('/');
        };

        $scope.submitForm = function (e, isDraft) {
            e.preventDefault();
            $scope.toBeSaved = angular.copy($scope.application);

            $scope.validateForm();
            if ($scope.toBeSaved.occupation) {
                $scope.toBeSaved.occupation = {
                    "occupationId": $scope.toBeSaved.occupation.occupationId
                };
            }
            if ($scope.toBeSaved.additionalBenefitsJsonKv) {
                $scope.toBeSaved.additionalBenefitsJsonKv = JSON.stringify($scope.toBeSaved.additionalBenefitsJsonKv);
                $scope.toBeSaved.additionalBenefitsJsonKv = $scope.toBeSaved.additionalBenefitsJsonKv.replace(/\x22/g, '\\\x22');
            }
            if ($scope.toBeSaved.plan) {
                $scope.toBeSaved.plan = $scope.toBeSaved.plan.planId + '';
            }
            if (isDraft) {
                $scope.toBeSaved.applicationStatus = 'DRAFT';
            } else {
                $scope.toBeSaved.applicationStatus = 'REGISTERED';
            }

            if ($scope.toBeSaved.birthDate) {
                var dd = $scope.toBeSaved.birthDate.getDate();
                var mm = $scope.toBeSaved.birthDate.getMonth() + 1; //January is 0!
                var yyyy = $scope.toBeSaved.birthDate.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }
                $scope.toBeSaved.birthDate = yyyy + '-' + mm + '-' + dd;
            }

            console.log("Submit", $scope.toBeSaved, $scope.applicationHTMLForm, $scope.paymentDeductions);

            if ($scope.paymentDeductions && $scope.paymentDeductions.length > 0) {
                $scope.toBeSaved.billingInformations = angular.copy($scope.paymentDeductions);
                for (var i = 0, len = $scope.toBeSaved.billingInformations.length; i < len; i++) {
                    $scope.toBeSaved.billingInformations[i].laborRegime += '';
                    $scope.toBeSaved.billingInformations[i].paymentUnit += '';
                    $scope.toBeSaved.billingInformations[i].paymentConcept += '';
                    $scope.toBeSaved.billingInformations[i].paymentPeriod += '';
                    $scope.toBeSaved.billingInformations[i].clabeAccountToken += '';
                    $scope.toBeSaved.billingInformations[i].paymentProposal += '';
                    if ($scope.toBeSaved.billingInformations[i].employer) {
                        $scope.toBeSaved.billingInformations[i].employer = {
                            employerId: $scope.toBeSaved.billingInformations[i].employer.employerId
                        };
                    }
                }
            }

            $rootScope.$emit('loadingOn');
            webServices.saveApplication($scope.toBeSaved, function (resp) {
                $rootScope.$emit('loadingOff');
                if (resp.statusText === 'Creado' && resp.data) {
                    $scope.$emit('alert', 'The application ' + resp.data.applicationId + ' was created successfully.');
                }
                if (resp.statusText === 'OK' && resp.data) {
                    $scope.$emit('alert', 'The application ' + resp.data.applicationId + ' was updated successfully.');
                }
                console.log(resp);
                $location.path('/');
            }, function (error) {
                $rootScope.$emit('loadingOff');

                console.log(error);
            })
        };


}]);
