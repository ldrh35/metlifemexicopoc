'use strict';

/**
 * @ngdoc overview
 * @name metLifeApp
 * @description
 * # metLifeApp
 *
 * Main module of the application.
 */
angular
    .module('metLifeApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'htmlToPdfSave'
  ])
    .config(['$locationProvider', '$routeProvider', '$httpProvider', function ($locationProvider, $routeProvider, $httpProvider) {

        $locationProvider.hashPrefix('');
        $routeProvider
            .when('/', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                controllerAs: 'main',
                resolve: {
                    user: ['webServices', '$q', '$location', '$resource', 'userInfo', function (webServices, $q, $location, $resource, userInfo) {
                        var dfd = $q.defer();
                        var user = userInfo.get();
                        if (!user) {
                            /*webServices.verifyUser({}, function (response) {
                              if (!response.user) {
                                $location.path('/login');
                                dfd.reject('Not logged');
                              } else {*/
                            userInfo.set({
                                name: 'testuser'
                            });
                            dfd.resolve();
                            /*    }
                              });*/
                        } else {
                            dfd.resolve();
                        }
                        return dfd.promise;
          }]
                }
            })
            .when('/form/:appId?', {
                templateUrl: 'views/mainform.html',
                controller: 'MainFormCtrl',
                controllerAs: 'mainform',
                resolve: {
                    applicationForm: ['webServices', '$q', '$rootScope', function (webServices, $q, $rootScope) {
                        var dfd = $q.defer();
                        $rootScope.$emit('loadingOn');
                        webServices.getApplicationForm({}, function (resp) {
                            $rootScope.$emit('loadingOff');
                            dfd.resolve(resp);
                        }, function () {
                            $rootScope.$emit('loadingOff');
                            dfd.reject('Error loading form values');
                        });

                        return dfd.promise;
                    }],
                    user: ['webServices', '$q', '$location', '$resource', 'userInfo', function (webServices, $q, $location, $resource, userInfo) {
                        /*var dfd = $q.defer();
                        var user = userInfo.get();
                        if (!user) {
                            webServices.verifyUser({}, function (response) {
                                if (!response.user) {
                                    $location.path('/login');
                                    dfd.reject('Not logged');
                                } else {
                                    userInfo.set({
                                        name: 'testuser'
                                    });
                                    dfd.resolve();
                                }
                            });
                        } else {
                            dfd.resolve();
                        }
                        return dfd.promise;*/
                        return true;
                    }],
                    applicationInfo: ['webServices', '$q', '$rootScope', '$route', function (webServices, $q, $rootScope, $route) {
                        var dfd = $q.defer();

                        if ($route.current.params.appId) {
                            $rootScope.$emit('loadingOn');
                            webServices.getApplicationInfo($route.current.params.appId, function (response) {
                                $rootScope.$emit('loadingOff');
                                console.log(response.data);
                                dfd.resolve(response.data);
                            }, function () {
                                $rootScope.$emit('loadingOff');
                            });
                        } else {
                            dfd.resolve(false);
                        }
                        return dfd.promise;
                    }]
                }
            })
            .when('/login', {
                templateUrl: 'views/login.html',
                controller: 'LoginCtrl',
                controllerAs: 'login',
                resolve: {
                    user: ['webServices', '$q', '$location', '$resource', 'userInfo', function (webServices, $q, $location, $resource, userInfo) {
                        var dfd = $q.defer();
                        /*var user = userInfo.get();
                        if (!user) {
                            webServices.verifyUser({}, function (response) {
                                if (!response.user) {
                                    $location.path('/login');
                                    dfd.reject('Not logged');
                                } else {
                                    dfd.resolve();
                                }
                            });
                        } else {*/
                        dfd.resolve();
                        //}
                        return dfd.promise;
                    }]
                }
            })
            .otherwise({
                redirectTo: '/login'
            });
  }]);
