package mx.com.metlife.dao;

import java.io.Serializable;

import org.hibernate.SessionFactory;

public interface IGenericDao<T, PK extends Serializable> {
	
	public void setSessionFactoryApp(SessionFactory sf) ;
	
	public void setType(Class<T> type) ;
	
	public T get(PK id) ;
	
	public T save (T object);
	
	public boolean exist(PK id);
	
	public boolean remove(T object);
	
	public T update(T object);

}
