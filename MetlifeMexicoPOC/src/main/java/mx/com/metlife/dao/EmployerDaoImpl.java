package mx.com.metlife.dao;

import java.util.List;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import mx.com.metlife.model.Employer;

@Repository
public class EmployerDaoImpl extends GenericHibernateDao<Employer, Integer> implements EmployerDao {
	
//	@Autowired
//	private SessionFactory sessionFactory;
		
//	public void setSessionFactory(SessionFactory sf){
//		this.sessionFactory = sf;
//	}
	
//	public Session getSession(){
//		return this.sessionFactory.getCurrentSession();
//	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Employer> search(String filter) {
		List<Employer> results;
		
		results = getSession().createCriteria(Employer.class)
		.add(Restrictions.or(
				Restrictions.ilike("employerName", filter, MatchMode.ANYWHERE), 
				Restrictions.ilike("employerRfc", filter, MatchMode.ANYWHERE)))
				.setMaxResults(20)
				.addOrder(Order.asc("employerName")).list();
		return results;
	}

}
