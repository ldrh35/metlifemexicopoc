package mx.com.metlife.dao;

import org.hibernate.FlushMode;
import org.springframework.stereotype.Repository;

import mx.com.metlife.model.Application;

@Repository
public class ApplicationDaoImpl extends GenericHibernateDao<Application, Integer> implements ApplicationDao {

	
	@Override
	public Application saveApplication(Application application) {
		getSession().merge(application);
		return application;
	}
	
	public Application storeProcedureOnCustomer(Application application) {
		String queryString = "EXECUTE CustomerUpdateAdd " + application.getRfc() + " , " + application.getCurp();
		Integer customerId = (Integer) getSession().createSQLQuery(queryString).uniqueResult();
		application.setCustomerIdNumber(customerId.toString());
		return application;
	}

	@Override
	public Application createApplication(Application application) {
		getSession().saveOrUpdate(application);
		getSession().setFlushMode(FlushMode.COMMIT);
		getSession().flush();
		
		//return storeProcedureOnCustomer(application);
		return get(application.getApplicationId());
	}

	@Override
	public void deleteApplication(Integer id) {
		Application application = (Application) getSession().load(Application.class, id);
		if(application != null){
			getSession().delete(application);
		}
	}
	

}
