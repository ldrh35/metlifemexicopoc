package mx.com.metlife.dao;

import java.util.List;

import mx.com.metlife.model.AdditionalBenefit;

public interface AdditionalBenefitDao {

	public AdditionalBenefit addAdditionalBenefit(AdditionalBenefit additionalBen);//C
	public List<AdditionalBenefit> getAdditionalBenefits();//R
	public AdditionalBenefit updateAdditionalBenefit(AdditionalBenefit additionalBen); //U
	public void deleteAdditionalBenefit(AdditionalBenefit additionalBen); //D
}
