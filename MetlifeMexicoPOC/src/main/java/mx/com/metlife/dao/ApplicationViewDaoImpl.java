package mx.com.metlife.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.com.metlife.model.ApplicationSearchView;

@Repository
public class ApplicationViewDaoImpl implements ApplicationViewDao {
	
	@Autowired
	private SessionFactory sessionFactory;
		
		public void setSessionFactory(SessionFactory sf){
			this.sessionFactory = sf;
		}
		
		public Session getSession(){
			return this.sessionFactory.getCurrentSession();
		}

		@SuppressWarnings("unchecked")
		@Override
		public List<ApplicationSearchView> getAllApplicationDetail() {
			List<ApplicationSearchView> results;
			results = getSession().createCriteria(ApplicationSearchView.class).list();
			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		public List<ApplicationSearchView> searchApplications(String filter) {
			List<ApplicationSearchView> results;
			
			results = getSession().createCriteria(ApplicationSearchView.class)
			.add(Restrictions.or(
					Restrictions.ilike("aplicationId", filter, MatchMode.ANYWHERE), 
					Restrictions.ilike("holderName", filter, MatchMode.ANYWHERE)))
					.setMaxResults(20)
					.addOrder(Order.desc("aplicationId")).list();
			return results;
		}

}
