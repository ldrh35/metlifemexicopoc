package mx.com.metlife.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.com.metlife.model.Occupation;

@Repository
public class OccupationDaoImpl implements OccupationDao {
	
	@Autowired
	private SessionFactory sessionFactory;
		
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	public Session getSession(){
		return this.sessionFactory.getCurrentSession();
	}

	@Override
	public Occupation getById(Integer occupationId) {
		Occupation occupation;
		occupation = (Occupation) getSession().createCriteria(Occupation.class)
			.add(Restrictions.eq("occupationId", occupationId)).list().get(0);
		return occupation;
	}

	@Override
	public Occupation getByCode(String code) {
		Occupation occupation;
		occupation = (Occupation) getSession().createCriteria(Occupation.class)
			.add(Restrictions.eq("ocupationCode", code)).list().get(0);
		return occupation;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Occupation> searchOccupationsByCodeOrName(String filter) {
		List<Occupation> results;
		
		results = getSession().createCriteria(Occupation.class)
		.add(Restrictions.or(
				Restrictions.ilike("ocupationCode", filter, MatchMode.ANYWHERE), 
				Restrictions.ilike("ocupationName", filter, MatchMode.ANYWHERE)))
				.setMaxResults(20)
				.addOrder(Order.asc("ocupationName")).list();
		return results;
	}

}
