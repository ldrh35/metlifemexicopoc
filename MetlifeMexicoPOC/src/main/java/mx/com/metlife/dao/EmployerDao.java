package mx.com.metlife.dao;

import java.util.List;

import mx.com.metlife.model.Employer;

public interface EmployerDao extends IGenericDao<Employer, Integer> {
	
	public List<Employer> search(String filter);

}
