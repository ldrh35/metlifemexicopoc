package mx.com.metlife.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.com.metlife.model.PlanBenefitRelated;

@Repository
public class PlanBenefitRelatedDaoImpl implements PlanBenefitRelatedDao {
	
	@Autowired
	private SessionFactory sessionFactory;
		
		public void setSessionFactory(SessionFactory sf){
			this.sessionFactory = sf;
		}
		
		public Session getSession(){
			return this.sessionFactory.getCurrentSession();
		}

	@SuppressWarnings("unchecked")
	@Override
	public List<PlanBenefitRelated> getPlanBenefitRelated() {
		return (List<PlanBenefitRelated>) getSession().createCriteria(PlanBenefitRelated.class).list();
	}

	@Override
	public PlanBenefitRelated createPlanBenefitRelated(PlanBenefitRelated planBenefitRelated) {
		getSession().saveOrUpdate(planBenefitRelated);
		return planBenefitRelated;
	}

}
