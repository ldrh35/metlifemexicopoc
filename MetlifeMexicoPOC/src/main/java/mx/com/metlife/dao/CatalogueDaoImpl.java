package mx.com.metlife.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.com.metlife.model.Catalogue;

@Repository
public class CatalogueDaoImpl implements CatalogueDao {
	
	@Autowired
	private SessionFactory sessionFactory;
		
		public void setSessionFactory(SessionFactory sf){
			this.sessionFactory = sf;
		}
		
		private Session getSession(){
			return this.sessionFactory.getCurrentSession();
		}

		@Override
		public Catalogue getById(Integer catalogueId) {
			Catalogue catalogue;
			
			catalogue = (Catalogue) getSession().createCriteria(Catalogue.class)
				.add(Restrictions.eq("catalogueId", catalogueId)).list().get(0);
			return catalogue;
		}


		@SuppressWarnings("unchecked")
		@Override
		public List<Catalogue> getCataloguesByParentId(Integer parentId) {
			List<Catalogue> results;
			
			results = getSession().createCriteria(Catalogue.class)
			.add(Restrictions.eq("parentId", parentId))
			.addOrder(Order.asc("catalogueOrder")).list();
			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		public List<Catalogue> getAllCatalogues() {
			List<Catalogue> results;
			results = getSession().createCriteria(Catalogue.class).list();
			return results;
		}
		
		@SuppressWarnings("unchecked")
		public List<Catalogue> getCataloguesByParentIdList(List<Integer> parentIdList) {
			List<Catalogue> results;
			results = getSession().createCriteria(Catalogue.class)
					.add(Restrictions.in("parentId", parentIdList)).list();
			return results;
		}
}
