package mx.com.metlife.dao;

import java.util.List;

import mx.com.metlife.model.Plan;

public interface PlanDao {

	public List<Plan> getPlans();
}
