package mx.com.metlife.dao;

import java.util.List;

import mx.com.metlife.model.PlanBenefitRelated;

public interface PlanBenefitRelatedDao {
	
	public List<PlanBenefitRelated> getPlanBenefitRelated();
	public PlanBenefitRelated createPlanBenefitRelated(PlanBenefitRelated planBenefitRelated);

}
