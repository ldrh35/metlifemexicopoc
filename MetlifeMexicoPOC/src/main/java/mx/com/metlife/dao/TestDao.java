package mx.com.metlife.dao;

import mx.com.metlife.model.TestTable;

public interface TestDao {

	public void addTest(TestTable test);
}
