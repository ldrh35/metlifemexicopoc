package mx.com.metlife.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.com.metlife.model.Plan;

@Repository
public class PlanDaoImpl implements PlanDao {
	
	@Autowired
	private SessionFactory sessionFactory;
		
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	public Session getSession(){
		return this.sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Plan> getPlans() {
		List<Plan> results;
		results = getSession().createCriteria(Plan.class).list();
		return results;
	}

}
