package mx.com.metlife.dao;

import java.util.List;

import mx.com.metlife.model.ApplicationSearchView;

public interface ApplicationViewDao {

	public List<ApplicationSearchView> getAllApplicationDetail();
	public List<ApplicationSearchView> searchApplications(String filter);
	
}
