package mx.com.metlife.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.com.metlife.model.AdditionalBenefit;

@Repository
public class AdditionalBenefitDaoImpl implements AdditionalBenefitDao {
	
	@Autowired
	private SessionFactory sessionFactory;
		
		public void setSessionFactory(SessionFactory sf){
			this.sessionFactory = sf;
		}
		
		public Session getSession(){
			return this.sessionFactory.getCurrentSession();
		}

	@Override
	public AdditionalBenefit addAdditionalBenefit(AdditionalBenefit additionalBen) {
		getSession().saveOrUpdate(additionalBen);
		return additionalBen;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AdditionalBenefit> getAdditionalBenefits() {
		return (List<AdditionalBenefit>) getSession().createCriteria(AdditionalBenefit.class)
				.addOrder(Order.asc("benefitOrder")).list();
	}

	@Override
	public AdditionalBenefit updateAdditionalBenefit(AdditionalBenefit additionalBen) {
		getSession().merge(additionalBen);
		return additionalBen;
	}

	@Override
	public void deleteAdditionalBenefit(AdditionalBenefit additionalBen) {
		getSession().delete(additionalBen);
	}

}
