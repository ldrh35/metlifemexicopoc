package mx.com.metlife.dao;

import java.util.List;

import mx.com.metlife.model.Occupation;

public interface OccupationDao {

	public Occupation getById(Integer occupationId);
	public Occupation getByCode(String code);
	public List<Occupation> searchOccupationsByCodeOrName(String filter);
		
}
