package mx.com.metlife.dao;

import java.util.List;

import mx.com.metlife.model.PrivacyLog;

public interface PrivacyLogDao {
	
	public PrivacyLog addPrivacyLog(PrivacyLog privacyLog);
	public List<PrivacyLog> getPrivacyLogLastMonth();
	public List<PrivacyLog> getPrivacyLogByApplicationId(Integer applicationId);
	public List<PrivacyLog> getPrivacyLogByCustomerId(Integer customerId);

}
