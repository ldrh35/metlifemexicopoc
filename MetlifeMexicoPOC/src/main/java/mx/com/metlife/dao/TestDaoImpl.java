package mx.com.metlife.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.com.metlife.model.TestTable;

@Repository
public class TestDaoImpl implements TestDao {

	@Autowired
private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public void addTest(TestTable test) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(test);
	}

}
