package mx.com.metlife.dao;

import mx.com.metlife.model.Application;

public interface ApplicationDao extends IGenericDao<Application, Integer> {
	
	public Application saveApplication(Application application);
	public Application createApplication(Application application);
	public void deleteApplication(Integer id);

}
