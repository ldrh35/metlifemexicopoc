package mx.com.metlife.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.com.metlife.model.PrivacyLog;

@Repository
public class PrivacyLogDaoImpl implements PrivacyLogDao {

	@Autowired
	private SessionFactory sessionFactory;
		
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	public Session getSession(){
		return this.sessionFactory.getCurrentSession();
	}
	
	@Override
	public PrivacyLog addPrivacyLog(PrivacyLog privacyLog) {
		getSession().saveOrUpdate(privacyLog);
		return privacyLog;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrivacyLog> getPrivacyLogLastMonth() {
		List<PrivacyLog> results;
		
		Date currentDate = new Date();
		Calendar c = Calendar.getInstance(); 
		c.setTime(currentDate); 
		c.add(Calendar.MONTH, -1);
		Date lastMonth = c.getTime();
		
		results = getSession().createCriteria(PrivacyLog.class)
				.add(Restrictions.between("eventDate", currentDate, lastMonth))
				.addOrder(Order.asc("eventDate")).list();
		return results;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrivacyLog> getPrivacyLogByApplicationId(Integer applicationId) {
		List<PrivacyLog> results;
		
		results = getSession().createCriteria(PrivacyLog.class)
				.add(Restrictions.eq("applicationId", applicationId))
				.addOrder(Order.asc("eventDate")).list();
		return results;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PrivacyLog> getPrivacyLogByCustomerId(Integer customerId) {
		List<PrivacyLog> results;
		
		results = getSession().createCriteria(PrivacyLog.class)
				.add(Restrictions.eq("customerId", customerId))
				.addOrder(Order.asc("eventDate")).list();
		return results;
	}

}
