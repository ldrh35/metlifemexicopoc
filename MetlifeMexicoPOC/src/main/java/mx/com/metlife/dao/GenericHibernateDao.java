package mx.com.metlife.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

import javax.persistence.EntityNotFoundException;

import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;

public class GenericHibernateDao<T, PK extends Serializable> extends HibernateDaoSupport implements IGenericDao<T, PK> {
	
	@Autowired
	public void setSessionFactoryApp(SessionFactory sf){
		setSessionFactory(sf);
	}
	
	protected Session getSession(){
		return getSessionFactory().getCurrentSession();
	}
	
	protected Class<T> type;

	public void setType(Class<T> type) {
		this.type = type;
	}
	
	@SuppressWarnings("unchecked")
	public GenericHibernateDao() {
		Class<T> type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
			.getActualTypeArguments()[0];
		this.type = type;
	}
	
	@SuppressWarnings("unchecked")
	public T get(PK id) {
		T entity = (T) getSession().get(type, id);
		return entity;
	}
	
	@SuppressWarnings("unchecked")
	public T save (T object) {
		PK id = (PK) getSession().save(object);
		getSession().setFlushMode(FlushMode.COMMIT);
		getSession().flush();
		return get(id);
	}
	
	@SuppressWarnings("unchecked")
	public boolean exist(PK id) {
		T entity = (T) getSession().get(type, id);
		return entity != null;
	}
	
	public boolean remove(T object) {
		try {
			getSession().delete(object);
			getSession().flush();
		}
		catch (HibernateException ex) {
			return false;
		}
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public boolean remove(PK id) {
		T object = (T) getSession().get(type, id);
		if (object == null) {
			throw new EntityNotFoundException();
		}
		boolean hasBeenRemoved = remove(object);
		return hasBeenRemoved;
	}
	
	public T update(T object) {
		getSession().update(object);
		getSession().flush();
		return object;
	}

}
