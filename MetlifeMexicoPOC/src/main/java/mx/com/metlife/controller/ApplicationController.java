package mx.com.metlife.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.com.metlife.model.Application;
import mx.com.metlife.model.ApplicationSearchView;
import mx.com.metlife.model.BillingInformation;
import mx.com.metlife.service.ApplicationService;
import mx.com.metlife.service.ApplicationViewService;


@RestController
public class ApplicationController {
	
	private ApplicationService applicationService;
	private ApplicationViewService applicationViewService;

	@Autowired
	public void setApplicationService(ApplicationService applicationService) {
		this.applicationService = applicationService;
	}
	
	@Autowired
	public void setApplicationViewService(ApplicationViewService applicationViewService) {
		this.applicationViewService = applicationViewService;
	}
	
	@RequestMapping(value="/applications", method=RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<List<ApplicationSearchView>> searchApplications(@RequestParam(value="query", required=false) String query) {
		if (query == null || query.isEmpty()) {
			return new ResponseEntity<List<ApplicationSearchView>>(this.applicationViewService.getAllApplicationDetail(),HttpStatus.OK);
		}
		else {
			return new ResponseEntity<List<ApplicationSearchView>>(this.applicationViewService.searchApplications(query),HttpStatus.OK);
		}
	}
	
	@RequestMapping(value="/applications/{id}", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Application> getById(@PathVariable Integer id) {
		return new ResponseEntity<Application>(applicationService.get(id), HttpStatus.OK);
	}
	
//	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
//	public void deleteApplication(@PathVariable Integer id) {
//		this.applicationService.deleteApplication(id);
//	}
	
	@RequestMapping(value="/applications", method=RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Application> addApplication(@RequestBody Application application){
		application.setApplicationDate(new Date());
		for(BillingInformation billInfo: application.getBillingInformations()){
			billInfo.setApplication(application);
		}
		return new ResponseEntity<Application>(this.applicationService.createApplication(application),HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/applications", method=RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Application> updateApplication(@RequestBody Application application){
		return new ResponseEntity<Application>(this.applicationService.saveApplication(application),HttpStatus.OK);
	}
	
}
