package mx.com.metlife.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import mx.com.metlife.model.TestTable;
import mx.com.metlife.service.TestService;

@RestController
@RequestMapping(value = "/testing",
        method = {RequestMethod.GET, RequestMethod.POST})
//@Configuration
//@ComponentScan("mx.com.metlife.service")
public class TestController {
	
	private TestService testService;

	@Autowired
	public void setTestService(TestService testService) {
		this.testService = testService;
	}

//	@RequestMapping(value="/employer", method=RequestMethod.POST)
//	public String createEmployer(@RequestBody MtfEmployer employer){
////		MtfCustomerPersonalInformation personalInfo = new MtfCustomerPersonalInformation(employer, mtfOccupationData, "Contreras", "Diana", "COVD870714", 
////				new Date(), "tester", 1.65, 60, "COVD870714", "F", "single", "100", "100", "Guadalajara", "Mexican", "test", 3, "test", 44790, "Ya�ez", "Guadalajara", "Guadalajara", "Jalisco", cty, cellPhoneNumber, phoneNumber, email, additionalBenefits)
//		Set<MtfCustomerPersonalInformation> personal = new HashSet<MtfCustomerPersonalInformation>();
//		employer.setMtfCustomerPersonalInformations(personal);
//		this.employerService.addEmployer(employer);
//		return "success!";
//	}
	
	@RequestMapping(method=RequestMethod.GET)
	public TestTable testDiana(@RequestBody TestTable test){
		this.testService.addTest(test);
		return test;
	}
}
