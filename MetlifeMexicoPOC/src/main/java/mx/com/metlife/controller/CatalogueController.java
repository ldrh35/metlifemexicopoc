package mx.com.metlife.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.com.metlife.jsonmodel.ApplicationFormJSON;
import mx.com.metlife.model.Catalogue;
import mx.com.metlife.service.CatalogueService;

@RestController
public class CatalogueController {
	
	private Integer STATES = 1;
	private Integer LABOR_REGIMENS = 39;
	private Integer GENERES = 42;
	private Integer MARITAL_STATUS = 45;
	private Integer PAYMENT_DEDUCTIONS = 56;
	private Integer PAYMENT_FREQUENCY = 59;
	private Integer PAYMENT_METHOD = 66;
	private Integer PRIORITIES = 71;
	private Integer APPLICATION_STATUSES = 75;
	private Integer BANKS = 81;
	
	private CatalogueService catalogueService;

	@Autowired
	public void setCatalogueService(CatalogueService catalogueService) {
		this.catalogueService = catalogueService;
	}

	@RequestMapping(value="/catalogues/{id}", method=RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Catalogue> getCatalogueById(@PathVariable Integer id){
		return new ResponseEntity<Catalogue>(this.catalogueService.getById(id), HttpStatus.OK);
	}
	
	@RequestMapping(value="/catalogues/{id}/children", method=RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<List<Catalogue>> getCataloguesByParentId(@PathVariable Integer id){
		return new ResponseEntity<List<Catalogue>>(this.catalogueService.getCataloguesByParentId(id), HttpStatus.OK);
	}
	
	@RequestMapping(value="/catalogues", method=RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<ApplicationFormJSON> getCataloguesByParentId(){
		ApplicationFormJSON json = new ApplicationFormJSON();
		
		json.setStates(catalogueService.getCataloguesByParentId(STATES));
		json.setLaborRegimens(catalogueService.getCataloguesByParentId(LABOR_REGIMENS));
		json.setGenders(catalogueService.getCataloguesByParentId(GENERES));
		json.setMaritalStatus(catalogueService.getCataloguesByParentId(MARITAL_STATUS));
		json.setPaymentDeductions(catalogueService.getCataloguesByParentId(PAYMENT_DEDUCTIONS));
		json.setPaymentFrequency(catalogueService.getCataloguesByParentId(PAYMENT_FREQUENCY));
		json.setPaymentMethod(catalogueService.getCataloguesByParentId(PAYMENT_METHOD));
		json.setPriorities(catalogueService.getCataloguesByParentId(PRIORITIES));
		json.setBanks(catalogueService.getCataloguesByParentId(BANKS));
		json.setApplicationStatuses(catalogueService.getCataloguesByParentId(APPLICATION_STATUSES));
		return new ResponseEntity<ApplicationFormJSON>(json, HttpStatus.OK);
	}
	
	
}
