package mx.com.metlife.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import mx.com.metlife.jsonmodel.ApplicationFormJSON;
import mx.com.metlife.model.AdditionalBenefit;
import mx.com.metlife.model.Catalogue;
import mx.com.metlife.model.Plan;
import mx.com.metlife.model.PlanBenefitRelated;
import mx.com.metlife.service.AdditionalBenefitService;
import mx.com.metlife.service.CatalogueService;
import mx.com.metlife.service.PlanService;

@RestController
@RequestMapping(value = "/applicationForm", method = {RequestMethod.GET})
public class ApplicationFormController {
	
	private Integer STATES = 1;
	private Integer LABOR_REGIMENS = 39;
	private Integer GENERES = 42;
	private Integer MARITAL_STATUS = 45;
	private Integer PAYMENT_DEDUCTIONS = 56;
	private Integer PAYMENT_FREQUENCY = 59;
	private Integer PAYMENT_METHOD = 66;
	private Integer PRIORITIES = 71;
	private Integer APPLICATION_STATUSES = 75;
	private Integer BANKS = 81;
	
	
	
	private CatalogueService catalogueService;
	private AdditionalBenefitService additionalBenefitService;
	private PlanService planService;
	
	@Autowired
	public void setCatalogueService(CatalogueService catalogueService) {
		this.catalogueService = catalogueService;
	}
	
	@Autowired
	public void setAdditionalBenefitService(AdditionalBenefitService additionalBenefitService) {
		this.additionalBenefitService = additionalBenefitService;
	}
	
	@Autowired
	public void setPlanService(PlanService planService) {
		this.planService = planService;
	}

	@RequestMapping(method=RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ApplicationFormJSON getCataloguesByParentId() {
		
		List<Integer> parentIdList = new ArrayList<Integer>();
		parentIdList.add(STATES);
		parentIdList.add(LABOR_REGIMENS);
		parentIdList.add(GENERES);
		parentIdList.add(MARITAL_STATUS);
		parentIdList.add(PAYMENT_DEDUCTIONS);
		parentIdList.add(PAYMENT_FREQUENCY);
		parentIdList.add(PAYMENT_METHOD);
		parentIdList.add(PRIORITIES);
		parentIdList.add(BANKS);
		parentIdList.add(APPLICATION_STATUSES);
		
		List<Catalogue> catalogueList = catalogueService.getCataloguesByParentIdList(parentIdList);
		
		ApplicationFormJSON json = new ApplicationFormJSON();
		
		for (Catalogue cat : catalogueList) {
			if (cat.getParentId() == STATES) {
				json.getStates().add(cat);
			}
			else if (cat.getParentId() == LABOR_REGIMENS) {
				json.getLaborRegimens().add(cat);
			}
			else if (cat.getParentId() == GENERES) {
				json.getGenders().add(cat);
			}
			else if (cat.getParentId() == MARITAL_STATUS) {
				json.getMaritalStatus().add(cat);
			}
			else if (cat.getParentId() == PAYMENT_DEDUCTIONS) {
				json.getPaymentDeductions().add(cat);
			}
			else if (cat.getParentId() == PAYMENT_FREQUENCY) {
				json.getPaymentFrequency().add(cat);
			}
			else if (cat.getParentId() == PAYMENT_METHOD) {
				json.getPaymentMethod().add(cat);
			}
			else if (cat.getParentId() == PRIORITIES) {
				json.getPriorities().add(cat);
			}
			else if (cat.getParentId() == APPLICATION_STATUSES) {
				json.getApplicationStatuses().add(cat);
			}
			else if (cat.getParentId() == BANKS) {
				json.getBanks().add(cat);
			}
		}
		
		json.setAdditionalBenefits(getAdditionalBenefitsWithPlans());
		List<Plan> planList = planService.getPlans();
		removePlansIlegalFields(planList);
		json.setPlans(planList);
		return json;
	}
	
	
	private Set<AdditionalBenefit> getAdditionalBenefitsWithPlans() {
		Set<AdditionalBenefit> additionalBenefitsSet = new HashSet<>();
		additionalBenefitsSet.addAll(this.additionalBenefitService.getAdditionalBenefits());

		Iterator<AdditionalBenefit> it = additionalBenefitsSet.iterator();
		while (it.hasNext()) {
			AdditionalBenefit additionalBenefit = it.next();
			Set<Integer> plansIdList = new HashSet<Integer>();
			Iterator<PlanBenefitRelated> planBenefitsRelatedIterator = additionalBenefit.getPlanBenefitRelateds().iterator();
			while (planBenefitsRelatedIterator.hasNext()) {
				PlanBenefitRelated planBenefit = planBenefitsRelatedIterator.next();
				plansIdList.add(planBenefit.getPlan().getPlanId());
			}
			additionalBenefit.setPlan(plansIdList);
		}
		return additionalBenefitsSet;
	}
	
	private void removePlansIlegalFields(List<Plan> planList) {
		Iterator<Plan> it = planList.iterator();
		while(it.hasNext()) {
			Plan plan = it.next();
			plan.setPlanBenefitRelateds(null);
		}
	}
	
}
