package mx.com.metlife.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.com.metlife.model.PrivacyLog;
import mx.com.metlife.service.PrivacyLogService;

@RestController
public class PrivacyLogController {
	
	private PrivacyLogService privacyLogService;

	@Autowired
	public void setPrivacyLogService(PrivacyLogService privacyLogService) {
		this.privacyLogService = privacyLogService;
	}
	
	@RequestMapping(value="/privacyLogs", method=RequestMethod.POST, 
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Void> addPrivacyLog(@RequestBody PrivacyLog privacyLog) {
		privacyLog.setEventDate(new Date());
		this.privacyLogService.addPrivacyLog(privacyLog);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@RequestMapping(value="/privacyLogs/lastMonth",
			method=RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<List<PrivacyLog>> getPrivacyLogLastMonth() {
		return new ResponseEntity<List<PrivacyLog>>(this.privacyLogService.getPrivacyLogLastMonth(),HttpStatus.OK);
	}
	
	@RequestMapping(value="/privacyLogs/application/{applicationId}",
			method=RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<List<PrivacyLog>> getPrivacyLogByApplicationId(@PathVariable Integer applicationId) {
		return new ResponseEntity<List<PrivacyLog>>(this.privacyLogService.getPrivacyLogByApplicationId(applicationId),HttpStatus.OK);
	}
	
	@RequestMapping(value="/privacyLogs/customer/{customerid}",
			method=RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<List<PrivacyLog>> getPrivacyLogByCustomerId(@PathVariable Integer customerId) {
		return new ResponseEntity<List<PrivacyLog>>(this.privacyLogService.getPrivacyLogByCustomerId(customerId),HttpStatus.OK);
	}
	
}
