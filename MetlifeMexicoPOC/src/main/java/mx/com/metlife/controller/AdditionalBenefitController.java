package mx.com.metlife.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.com.metlife.model.AdditionalBenefit;
import mx.com.metlife.model.PlanBenefitRelated;
import mx.com.metlife.service.AdditionalBenefitService;
import mx.com.metlife.service.PlanBenefitRelatedService;

@RestController
public class AdditionalBenefitController {
	
	private AdditionalBenefitService additionalBenefitService;
	private PlanBenefitRelatedService planBenefitRelatedService;

	@Autowired
	public void setAdditionalBenefitService(AdditionalBenefitService additionalBenefitService) {
		this.additionalBenefitService = additionalBenefitService;
	}
	
	@Autowired
	public void setPlanBenefitRelatedService(PlanBenefitRelatedService planBenefitRelatedService){
		this.planBenefitRelatedService = planBenefitRelatedService;
	}
	
	@RequestMapping(value="/additionalBenefit", method=RequestMethod.POST, 
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<AdditionalBenefit> addAdditionalBenefit(@RequestBody AdditionalBenefit additionalBenefit){
		return new ResponseEntity<AdditionalBenefit>(this.additionalBenefitService.addAdditionalBenefit(additionalBenefit),HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/additionalBenefit", method=RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<List<AdditionalBenefit>> getAdditionalBenefit(){
		return new ResponseEntity<List<AdditionalBenefit>>(this.additionalBenefitService.getAdditionalBenefits(),HttpStatus.OK);
	}
	
	@RequestMapping(value="/additionalBenefit", method=RequestMethod.PUT,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<AdditionalBenefit> updateAdditionalBenefit(@RequestBody AdditionalBenefit additionalBen){
		return new ResponseEntity<AdditionalBenefit>(this.additionalBenefitService.updateAdditionalBenefit(additionalBen), HttpStatus.OK);
	}
	
	@RequestMapping(value="/additionalBenefit", method=RequestMethod.DELETE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Void> deleteAdditionalBenefit(@RequestBody AdditionalBenefit additionalBenefit){
		this.additionalBenefitService.deleteAdditionalBenefit(additionalBenefit);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@RequestMapping(value="/additionalBenefit/plans", method=RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<List<AdditionalBenefit>> getAdditionalBenefitWithPlan(){
		List<AdditionalBenefit> additionalBen = this.additionalBenefitService.getAdditionalBenefits();
		List<PlanBenefitRelated> planBenefitRelated = this.planBenefitRelatedService.getPlanBenefitRelated();
		for(PlanBenefitRelated planRel:planBenefitRelated){	
			for(AdditionalBenefit addBen:additionalBen){
				if(planRel.getAdditionalBenefit().getAdditionalBenefitId().equals(
				   addBen.getAdditionalBenefitId())){
						addBen.addPlan(planRel.getPlanBenefitRelatedId());
				}
			}
		}
		return new ResponseEntity<List<AdditionalBenefit>>(additionalBen, HttpStatus.OK);
	}
	
//	@RequestMapping(value="/additionalBenefit/plan", method=RequestMethod.POST, 
//			consumes = MediaType.APPLICATION_JSON_VALUE)
//	public @ResponseBody ResponseEntity<PlanBenefitRelated> addPlan(@RequestBody PlanBenefitRelated planBenefitRelated){
//		return new ResponseEntity<PlanBenefitRelated>(this.planBenefitRelatedService.createPlanBenefitRelated(planBenefitRelated),HttpStatus.CREATED);
//	}
}
