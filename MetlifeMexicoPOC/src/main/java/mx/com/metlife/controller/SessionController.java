package mx.com.metlife.controller;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.com.metlife.model.SessionAttributes;

@RestController
public class SessionController {

	@RequestMapping(value = "/sessionAttr", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<SessionAttributes> getSessionAttr(HttpSession session) {
		SessionAttributes sessionAttributes = new SessionAttributes();
		if (session.getAttribute("attributes") != null) {
			sessionAttributes.setAttributes(session.getAttribute("attributes"));
		}
		if (session.getAttribute("nameId") != null) {
			sessionAttributes.setNameId(session.getAttribute("nameId").toString());
		}
		return new ResponseEntity<SessionAttributes>(sessionAttributes, HttpStatus.OK);
	}

}
