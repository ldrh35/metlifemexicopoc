package mx.com.metlife.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.com.metlife.model.Employer;
import mx.com.metlife.service.EmployerService;

@RestController
public class EmployerController {
	
	private EmployerService employerService;

	@Autowired
	public void setEmployerService(EmployerService employerService) {
		this.employerService = employerService;
	}

	@RequestMapping(value = "/employers", method=RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<List<Employer>> search(@RequestParam(value="query", required=false) String query){
		return new ResponseEntity<List<Employer>>(this.employerService.search(query), HttpStatus.OK);
	}
	
}
