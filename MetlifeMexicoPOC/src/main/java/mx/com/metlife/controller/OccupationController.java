package mx.com.metlife.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import mx.com.metlife.model.Occupation;
import mx.com.metlife.service.OccupationService;

@RestController
public class OccupationController {
	
	private OccupationService occupationService;

	@Autowired
	public void setOccupationService(OccupationService occupationService) {
		this.occupationService = occupationService;
	}

	@RequestMapping(value="/occupations/{id}", method=RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Occupation> getCatalogueById(@PathVariable Integer id){
		return new ResponseEntity<Occupation>(this.occupationService.getById(id), HttpStatus.OK);
	}
	
	@RequestMapping(value="/occupations/{code}", method=RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Occupation> getCatalogueById(@PathVariable String code){
		return new ResponseEntity<Occupation>(this.occupationService.getByCode(code), HttpStatus.OK);
	}
		
	@RequestMapping(value="/occupations", method=RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<List<Occupation>> search(@RequestParam(value="query", required=false) String query){
		return new ResponseEntity<List<Occupation>>(this.occupationService.searchOccupationsByCodeOrName(query), HttpStatus.OK);
	}
	
}
