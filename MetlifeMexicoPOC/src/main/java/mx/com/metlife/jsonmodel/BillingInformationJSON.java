package mx.com.metlife.jsonmodel;

import mx.com.metlife.model.BillingInformation;

public class BillingInformationJSON implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer billingId;
	private String paymentMethod;
	private String federativeEntity;
	private String paymentUnit;
	private String paymentConcept;
	private String paymentPeriod;
	private String paymentType;
	private String laborRegime;
	private String clabeAccountToken;
	private String paymentProposal;
	private String bank;
	private String priority;

	public BillingInformationJSON() {
	}

	
	public BillingInformationJSON(BillingInformation billingInformation) {
		this.billingId = billingInformation.getBillingId();
		this.paymentMethod = billingInformation.getPaymentMethod();
		this.federativeEntity = billingInformation.getFederativeEntity();
		this.paymentUnit = billingInformation.getPaymentUnit();
		this.paymentConcept = billingInformation.getPaymentConcept();
		this.paymentPeriod = billingInformation.getPaymentPeriod();
		this.paymentType = billingInformation.getPaymentType();
		this.laborRegime = billingInformation.getLaborRegime();
		this.clabeAccountToken = billingInformation.getClabeAccountToken();
		this.paymentProposal = billingInformation.getPaymentProposal();
		this.bank = billingInformation.getBank();
		this.priority = billingInformation.getPriority();
	}

	public Integer getBillingId() {
		return this.billingId;
	}

	public void setBillingId(Integer pkBillingId) {
		this.billingId = pkBillingId;
	}

	public String getPaymentMethod() {
		return this.paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getFederativeEntity() {
		return this.federativeEntity;
	}

	public void setFederativeEntity(String federativeEntity) {
		this.federativeEntity = federativeEntity;
	}

	public String getPaymentUnit() {
		return this.paymentUnit;
	}

	public void setPaymentUnit(String paymentUnit) {
		this.paymentUnit = paymentUnit;
	}

	public String getPaymentConcept() {
		return this.paymentConcept;
	}

	public void setPaymentConcept(String paymentConcept) {
		this.paymentConcept = paymentConcept;
	}

	public String getPaymentPeriod() {
		return this.paymentPeriod;
	}

	public void setPaymentPeriod(String paymentPeriod) {
		this.paymentPeriod = paymentPeriod;
	}

	public String getPaymentType() {
		return this.paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getLaborRegime() {
		return this.laborRegime;
	}

	public void setLaborRegime(String laborRegime) {
		this.laborRegime = laborRegime;
	}

	public String getClabeAccountToken() {
		return this.clabeAccountToken;
	}

	public void setClabeAccountToken(String clabeAccountToken) {
		this.clabeAccountToken = clabeAccountToken;
	}

	public String getPaymentProposal() {
		return this.paymentProposal;
	}

	public void setPaymentProposal(String paymentProposal) {
		this.paymentProposal = paymentProposal;
	}

	public String getBank() {
		return this.bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public String getPriority() {
		return this.priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

}
