package mx.com.metlife.jsonmodel;
// Generated 2/06/2018 10:31:41 PM by Hibernate Tools 4.3.5.Final

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import mx.com.metlife.model.Application;
import mx.com.metlife.model.BillingInformation;

public class ApplicationJSON implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer applicationId;
	private Integer employerId;
	private String employerName;
	private String employerDescription;
	private String employerRfc;
	private Integer occupationId;
	private String ocupationCode;
	private String ocupationName;
	private String ocupationAlias;
	private String aliasDescription;
	private String lastName;
	private String MLastName;
	private String firstName;
	private String rfc;
	private Date birthDate;
	private double height;
	private double weight;
	private String curp;
	private String gender;
	private String maritalStatus;
	private String customerIdType;
	private String customerIdNumber;
	private String ctyBirth;
	private String nationalities;
	private String streetName;
	private String streetNumberInternal;
	private String streetNumberExternal;
	private String zipCode;
	private String neighborhood;
	private String municipality;
	private String city;
	private String state;
	private String country;
	private String cellPhoneNumber;
	private String homePhoneNumber;
	private String email;
	private String additionalBenefitsJsonKv;
	private String plan;
	private double amount;
	private Date applicationDate;
	private String applicationStatus;
	private Set<BillingInformationJSON> billingInformations = new HashSet<BillingInformationJSON>();
	
	public ApplicationJSON(Application application) {
		applicationId = application.getApplicationId();
//		employerId = application.getEmployer().getEmployerId();
//		employerName = application.getEmployer().getEmployerName();
//		employerDescription = application.getEmployer().getEmployerDescription();
//		employerRfc = application.getEmployer().getEmployerRfc();
		occupationId = application.getOccupation().getOccupationId();
		ocupationCode = application.getOccupation().getOcupationCode();
		ocupationName = application.getOccupation().getOcupationName();
		ocupationAlias = application.getOccupation().getOcupationAlias();
		aliasDescription = application.getOccupation().getAliasDescription();
		lastName = application.getLastName();
		MLastName = application.getMLastName();
		firstName = application.getFirstName();
		rfc = application.getRfc();
		birthDate = application.getBirthDate();
		height = application.getHeight();
		weight = application.getWeight();
		curp = application.getCurp();
		gender = application.getGender();
		maritalStatus = application.getMaritalStatus();
		customerIdType = application.getCustomerIdType();
		customerIdNumber = application.getCustomerIdNumber();
		ctyBirth = application.getCtyBirth();
		nationalities = application.getNationalities();
		streetName = application.getStreetName();
		streetNumberInternal = application.getStreetNumberInternal();
		streetNumberExternal = application.getStreetNumberExternal();
		zipCode = application.getZipCode();
		neighborhood = application.getNeighborhood();
		municipality = application.getMunicipality();
		city = application.getCity();
		state = application.getState();
		country = application.getCountry();
		cellPhoneNumber = application.getCellPhoneNumber();
		homePhoneNumber = application.getHomePhoneNumber();
		email = application.getEmail();
		additionalBenefitsJsonKv = application.getAdditionalBenefitsJsonKv();
		plan = application.getPlan();
		amount = application.getAmount();
		applicationDate = application.getApplicationDate();
		applicationStatus = application.getApplicationStatus();
		billingInformations = parseBillingInformation(application.getBillingInformations());
	}
	
	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getEmployerId() {
		return employerId;
	}

	public void setEmployerId(Integer employerId) {
		this.employerId = employerId;
	}

	public String getEmployerName() {
		return employerName;
	}

	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}

	public String getEmployerDescription() {
		return employerDescription;
	}

	public void setEmployerDescription(String employerDescription) {
		this.employerDescription = employerDescription;
	}

	public String getEmployerRfc() {
		return employerRfc;
	}

	public void setEmployerRfc(String employerRfc) {
		this.employerRfc = employerRfc;
	}

	public Integer getOccupationId() {
		return occupationId;
	}

	public void setOccupationId(Integer occupationId) {
		this.occupationId = occupationId;
	}

	public String getOcupationCode() {
		return ocupationCode;
	}

	public void setOcupationCode(String ocupationCode) {
		this.ocupationCode = ocupationCode;
	}

	public String getOcupationName() {
		return ocupationName;
	}

	public void setOcupationName(String ocupationName) {
		this.ocupationName = ocupationName;
	}

	public String getOcupationAlias() {
		return ocupationAlias;
	}

	public void setOcupationAlias(String ocupationAlias) {
		this.ocupationAlias = ocupationAlias;
	}

	public String getAliasDescription() {
		return aliasDescription;
	}

	public void setAliasDescription(String aliasDescription) {
		this.aliasDescription = aliasDescription;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMLastName() {
		return MLastName;
	}

	public void setMLastName(String mLastName) {
		MLastName = mLastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getCustomerIdType() {
		return customerIdType;
	}

	public void setCustomerIdType(String customerIdType) {
		this.customerIdType = customerIdType;
	}

	public String getCustomerIdNumber() {
		return customerIdNumber;
	}

	public void setCustomerIdNumber(String customerIdNumber) {
		this.customerIdNumber = customerIdNumber;
	}

	public String getCtyBirth() {
		return ctyBirth;
	}

	public void setCtyBirth(String ctyBirth) {
		this.ctyBirth = ctyBirth;
	}

	public String getNationalities() {
		return nationalities;
	}

	public void setNationalities(String nationalities) {
		this.nationalities = nationalities;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getStreetNumberInternal() {
		return streetNumberInternal;
	}

	public void setStreetNumberInternal(String streetNumberInternal) {
		this.streetNumberInternal = streetNumberInternal;
	}

	public String getStreetNumberExternal() {
		return streetNumberExternal;
	}

	public void setStreetNumberExternal(String streetNumberExternal) {
		this.streetNumberExternal = streetNumberExternal;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getNeighborhood() {
		return neighborhood;
	}

	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}

	public String getMunicipality() {
		return municipality;
	}

	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCellPhoneNumber() {
		return cellPhoneNumber;
	}

	public void setCellPhoneNumber(String cellPhoneNumber) {
		this.cellPhoneNumber = cellPhoneNumber;
	}

	public String getHomePhoneNumber() {
		return homePhoneNumber;
	}

	public void setHomePhoneNumber(String homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdditionalBenefitsJsonKv() {
		return additionalBenefitsJsonKv;
	}

	public void setAdditionalBenefitsJsonKv(String additionalBenefitsJsonKv) {
		this.additionalBenefitsJsonKv = additionalBenefitsJsonKv;
	}

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public Set<BillingInformationJSON> getBillingInformations() {
		return this.billingInformations;
	}

	public void setBillingInformations(Set<BillingInformationJSON> billingInformations) {
		this.billingInformations = billingInformations;
	}
	
	private Set<BillingInformationJSON> parseBillingInformation(Set<BillingInformation> set) {
		Set<BillingInformationJSON> billingInfoJSON = new HashSet<BillingInformationJSON>();
		for (BillingInformation bi : set) {
			billingInfoJSON.add(new BillingInformationJSON(bi));
		}
		return billingInfoJSON;
	}

}
 