package mx.com.metlife.jsonmodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import mx.com.metlife.model.AdditionalBenefit;
import mx.com.metlife.model.Catalogue;
import mx.com.metlife.model.Plan;

public class ApplicationFormJSON implements java.io.Serializable {

	private static final long serialVersionUID = -8508156454357875135L;
	
	private List<Catalogue> states;
	private List<Catalogue> laborRegimens;
	private List<Catalogue> genders;
	private List<Catalogue> maritalStatus;
	private List<Catalogue> paymentDeductions;
	private List<Catalogue> paymentFrequency;
	private List<Catalogue> paymentMethod;
	private List<Catalogue> priorities;
	private List<Catalogue> applicationStatuses;
	private List<Catalogue> banks;
	
	public ApplicationFormJSON() {
		states = new ArrayList<Catalogue>();
		laborRegimens = new ArrayList<Catalogue>();
		genders = new ArrayList<Catalogue>();
		maritalStatus = new ArrayList<Catalogue>();
		paymentDeductions = new ArrayList<Catalogue>();
		paymentFrequency = new ArrayList<Catalogue>();
		paymentMethod = new ArrayList<Catalogue>();
		priorities = new ArrayList<Catalogue>();
		applicationStatuses = new ArrayList<Catalogue>();
		banks = new ArrayList<Catalogue>();
	}
	
	private Set<AdditionalBenefit> additionalBenefits;
	private List<Plan> plans;
	
	public List<Catalogue> getStates() {
		return states;
	}
	public void setStates(List<Catalogue> states) {
		this.states = states;
	}
	public List<Catalogue> getLaborRegimens() {
		return laborRegimens;
	}
	public void setLaborRegimens(List<Catalogue> laborRegimens) {
		this.laborRegimens = laborRegimens;
	}
	public List<Catalogue> getGenders() {
		return genders;
	}
	public void setGenders(List<Catalogue> sexes) {
		this.genders = sexes;
	}
	public List<Catalogue> getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(List<Catalogue> maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public List<Catalogue> getPaymentDeductions() {
		return paymentDeductions;
	}
	public void setPaymentDeductions(List<Catalogue> paymentDeductions) {
		this.paymentDeductions = paymentDeductions;
	}
	public List<Catalogue> getPaymentFrequency() {
		return paymentFrequency;
	}
	public void setPaymentFrequency(List<Catalogue> paymentFrequency) {
		this.paymentFrequency = paymentFrequency;
	}
	public List<Catalogue> getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(List<Catalogue> paymentType) {
		this.paymentMethod = paymentType;
	}
	public List<Catalogue> getPriorities() {
		return priorities;
	}
	public void setPriorities(List<Catalogue> priorities) {
		this.priorities = priorities;
	}
	public List<Catalogue> getApplicationStatuses() {
		return applicationStatuses;
	}
	public void setApplicationStatuses(List<Catalogue> applicationStatuses) {
		this.applicationStatuses = applicationStatuses;
	}
	public List<Catalogue> getBanks() {
		return banks;
	}
	public void setBanks(List<Catalogue> banks) {
		this.banks = banks;
	}
	public Set<AdditionalBenefit> getAdditionalBenefits() {
		return additionalBenefits;
	}
	public void setAdditionalBenefits(Set<AdditionalBenefit> additionalBenefits) {
		this.additionalBenefits = additionalBenefits;
	}
	public List<Plan> getPlans() {
		return plans;
	}
	public void setPlans(List<Plan> plans) {
		this.plans = plans;
	}
	
}
