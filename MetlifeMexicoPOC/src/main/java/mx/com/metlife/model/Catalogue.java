package mx.com.metlife.model;
// Generated 2/06/2018 10:31:41 PM by Hibernate Tools 4.3.5.Final

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * MtfCatalogue generated by hbm2java
 */
@Entity
@Table(name = "MTF_Catalogue", schema = "dbo", catalog = "MTF_DB_DE", uniqueConstraints = @UniqueConstraint(columnNames = "catalogue_code"))
public class Catalogue implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer catalogueId;
	private String catalogueName;
	private String catalogueDescription;
	private String catalogueCode;
	private String catalogueValue;
	private Integer catalogueOrder;
	private Integer parentId;

	public Catalogue() {
	}

	public Catalogue(String catalogueName, String catalogueCode) {
		this.catalogueName = catalogueName;
		this.catalogueCode = catalogueCode;
	}

	public Catalogue(String catalogueName, String catalogueDescription, String catalogueCode, String catalogueValue,
			Integer catalogueOrder, Integer catalogueParentId) {
		this.catalogueName = catalogueName;
		this.catalogueDescription = catalogueDescription;
		this.catalogueCode = catalogueCode;
		this.catalogueValue = catalogueValue;
		this.catalogueOrder = catalogueOrder;
		this.parentId = catalogueParentId;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "PK_mtp_catalogue_id", unique = true, nullable = false)
	public Integer getCatalogueId() {
		return this.catalogueId;
	}

	public void setCatalogueId(Integer pkMtpCatalogueId) {
		this.catalogueId = pkMtpCatalogueId;
	}

	@Column(name = "catalogue_name", nullable = false, length = 40)
	public String getCatalogueName() {
		return this.catalogueName;
	}

	public void setCatalogueName(String catalogueName) {
		this.catalogueName = catalogueName;
	}

	@Column(name = "catalogue_description", length = 60)
	public String getCatalogueDescription() {
		return this.catalogueDescription;
	}

	public void setCatalogueDescription(String catalogueDescription) {
		this.catalogueDescription = catalogueDescription;
	}

	@Column(name = "catalogue_code", unique = true, nullable = false, length = 40)
	public String getCatalogueCode() {
		return this.catalogueCode;
	}

	public void setCatalogueCode(String catalogueCode) {
		this.catalogueCode = catalogueCode;
	}

	@Column(name = "catalogue_value", length = 40)
	public String getCatalogueValue() {
		return this.catalogueValue;
	}

	public void setCatalogueValue(String catalogueValue) {
		this.catalogueValue = catalogueValue;
	}

	@Column(name = "catalogue_order")
	public Integer getCatalogueOrder() {
		return this.catalogueOrder;
	}

	public void setCatalogueOrder(Integer catalogueOrder) {
		this.catalogueOrder = catalogueOrder;
	}

	@Column(name = "catalogue_parent_id")
	public Integer getParentId() {
		return this.parentId;
	}

	public void setParentId(Integer catalogueParentId) {
		this.parentId = catalogueParentId;
	}

}
