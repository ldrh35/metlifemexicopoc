package mx.com.metlife.model;

public class SessionAttributes {
	
	private Object attributes;
	private String nameId;
	
	public Object getAttributes() {
		return attributes;
	}
	public void setAttributes(Object attributes) {
		this.attributes = attributes;
	}
	public String getNameId() {
		return nameId;
	}
	public void setNameId(String nameId) {
		this.nameId = nameId;
	}

}
