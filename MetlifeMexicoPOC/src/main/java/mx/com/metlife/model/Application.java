package mx.com.metlife.model;
// Generated 2/06/2018 10:31:41 PM by Hibernate Tools 4.3.5.Final

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * MtfApplication generated by hbm2java
 */
@Entity
@Table(name = "MTF_Application", schema = "dbo", catalog = "MTF_DB_DE")
public class Application implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer applicationId;
	private Occupation occupation;
	private String lastName;
	private String MLastName;
	private String firstName;
	private String rfc;
	private Date birthDate;
	private double height;
	private double weight;
	private String curp;
	private String gender;
	private String maritalStatus;
	private String customerIdType;
	private String customerIdNumber;
	private String ctyBirth;
	private String nationalities;
	private String streetName;
	private String streetNumberInternal;
	private String streetNumberExternal;
	private String zipCode;
	private String neighborhood;
	private String municipality;
	private String city;
	private String state;
	private String country;
	private String cellPhoneNumber;
	private String homePhoneNumber;
	private String email;
	private String additionalBenefitsJsonKv;
	private String plan;
	private double amount;
	private Date applicationDate;
	private String applicationStatus;
	private Set<BillingInformation> billingInformations = new HashSet<BillingInformation>(0);

	public Application() {
	}

	public Application(Occupation occupation, String lastName, String firstName,
			String rfc, Date birthDate, double height, double weight, String curp, String gender, String maritalStatus,
			String customerIdType, String customerIdNumber, String ctyBirth, String nationalities, String streetName,
			String streetNumberInternal, String streetNumberExternal, String zipCode, String neighborhood,
			String municipality, String city, String state, String country, String cellPhoneNumber, String homePhoneNumber,
			String email, String additionalBenefitsJsonKv, String planRel, double amount, Date applicationDate,
			String applicationStatus) {
		this.occupation = occupation;
		this.lastName = lastName;
		this.firstName = firstName;
		this.rfc = rfc;
		this.birthDate = birthDate;
		this.height = height;
		this.weight = weight;
		this.curp = curp;
		this.gender = gender;
		this.maritalStatus = maritalStatus;
		this.customerIdType = customerIdType;
		this.customerIdNumber = customerIdNumber;
		this.ctyBirth = ctyBirth;
		this.nationalities = nationalities;
		this.streetName = streetName;
		this.streetNumberInternal = streetNumberInternal;
		this.streetNumberExternal = streetNumberExternal;
		this.zipCode = zipCode;
		this.neighborhood = neighborhood;
		this.municipality = municipality;
		this.city = city;
		this.state = state;
		this.country = country;
		this.cellPhoneNumber = cellPhoneNumber;
		this.homePhoneNumber = homePhoneNumber;
		this.email = email;
		this.additionalBenefitsJsonKv = additionalBenefitsJsonKv;
		this.plan = planRel;
		this.amount = amount;
		this.applicationDate = applicationDate;
		this.applicationStatus = applicationStatus;
	}

	public Application(Occupation occupation, String lastName, String MLastName,
			String firstName, String rfc, Date birthDate, double height, double weight, String curp, String gender,
			String maritalStatus, String customerIdType, String customerIdNumber, String ctyBirth, String nationalities,
			String streetName, String streetNumberInternal, String streetNumberExternal, String zipCode, String neighborhood,
			String municipality, String city, String state, String country, String cellPhoneNumber, String homePhoneNumber,
			String email, String additionalBenefitsJsonKv, String planRel, double amount, Date applicationDate,
			String applicationStatus, Set<BillingInformation> billingInformations) {
		this.occupation = occupation;
		this.lastName = lastName;
		this.MLastName = MLastName;
		this.firstName = firstName;
		this.rfc = rfc;
		this.birthDate = birthDate;
		this.height = height;
		this.weight = weight;
		this.curp = curp;
		this.gender = gender;
		this.maritalStatus = maritalStatus;
		this.customerIdType = customerIdType;
		this.customerIdNumber = customerIdNumber;
		this.ctyBirth = ctyBirth;
		this.nationalities = nationalities;
		this.streetName = streetName;
		this.streetNumberInternal = streetNumberInternal;
		this.streetNumberExternal = streetNumberExternal;
		this.zipCode = zipCode;
		this.neighborhood = neighborhood;
		this.municipality = municipality;
		this.city = city;
		this.state = state;
		this.country = country;
		this.cellPhoneNumber = cellPhoneNumber;
		this.homePhoneNumber = homePhoneNumber;
		this.email = email;
		this.additionalBenefitsJsonKv = additionalBenefitsJsonKv;
		this.plan = planRel;
		this.amount = amount;
		this.applicationDate = applicationDate;
		this.applicationStatus = applicationStatus;
		this.billingInformations = billingInformations;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "PK_mtp_application_id", unique = true, nullable = false)
	public Integer getApplicationId() {
		return this.applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	@ManyToOne(fetch = FetchType.EAGER,cascade=CascadeType.ALL)
	@JoinColumn(name = "FK_mtp_occupation_id", nullable = false)
	public Occupation getOccupation() {
		return this.occupation;
	}

	public void setOccupation(Occupation occupation) {
		this.occupation = occupation;
	}

	@Column(name = "last_name", nullable = false, length = 40)
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "m_last_name", length = 40)
	public String getMLastName() {
		return this.MLastName;
	}

	public void setMLastName(String MLastName) {
		this.MLastName = MLastName;
	}

	@Column(name = "first_name", nullable = false, length = 40)
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "rfc", nullable = false, length = 13)
	public String getRfc() {
		return this.rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "birth_date", nullable = false, length = 10)
	public Date getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	@Column(name = "height", nullable = false, precision = 53, scale = 0)
	public double getHeight() {
		return this.height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	@Column(name = "weight", nullable = false, precision = 53, scale = 0)
	public double getWeight() {
		return this.weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	@Column(name = "curp", nullable = false, length = 18)
	public String getCurp() {
		return this.curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	@Column(name = "gender", nullable = false, length = 20)
	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Column(name = "marital_status", nullable = false, length = 20)
	public String getMaritalStatus() {
		return this.maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	@Column(name = "customer_id_type", nullable = false, length = 20)
	public String getCustomerIdType() {
		return this.customerIdType;
	}

	public void setCustomerIdType(String customerIdType) {
		this.customerIdType = customerIdType;
	}

	@Column(name = "customer_id_number", nullable = false, length = 20)
	public String getCustomerIdNumber() {
		return this.customerIdNumber;
	}

	public void setCustomerIdNumber(String customerIdNumber) {
		this.customerIdNumber = customerIdNumber;
	}

	@Column(name = "cty_birth", nullable = false, length = 60)
	public String getCtyBirth() {
		return this.ctyBirth;
	}

	public void setCtyBirth(String ctyBirth) {
		this.ctyBirth = ctyBirth;
	}

	@Column(name = "nationalities", nullable = false, length = 60)
	public String getNationalities() {
		return this.nationalities;
	}

	public void setNationalities(String nationalities) {
		this.nationalities = nationalities;
	}

	@Column(name = "street_name", nullable = false, length = 60)
	public String getStreetName() {
		return this.streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	@Column(name = "street_number_internal", nullable = false)
	public String getStreetNumberInternal() {
		return this.streetNumberInternal;
	}

	public void setStreetNumberInternal(String streetNumberInternal) {
		this.streetNumberInternal = streetNumberInternal;
	}

	@Column(name = "street_number_external", nullable = false, length = 10)
	public String getStreetNumberExternal() {
		return this.streetNumberExternal;
	}

	public void setStreetNumberExternal(String streetNumberExternal) {
		this.streetNumberExternal = streetNumberExternal;
	}

	@Column(name = "zip_code", nullable = false)
	public String getZipCode() {
		return this.zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@Column(name = "neighborhood", nullable = false, length = 60)
	public String getNeighborhood() {
		return this.neighborhood;
	}

	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}

	@Column(name = "municipality", nullable = false, length = 40)
	public String getMunicipality() {
		return this.municipality;
	}

	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}

	@Column(name = "city", nullable = false, length = 40)
	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "state", nullable = false, length = 40)
	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "country", nullable = false, length = 40)
	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Column(name = "cell_phone_number", nullable = false)
	public String getCellPhoneNumber() {
		return this.cellPhoneNumber;
	}

	public void setCellPhoneNumber(String cellPhoneNumber) {
		this.cellPhoneNumber = cellPhoneNumber;
	}

	@Column(name = "home_phone_number", nullable = false)
	public String getHomePhoneNumber() {
		return this.homePhoneNumber;
	}

	public void setHomePhoneNumber(String homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}

	@Column(name = "email", nullable = false, length = 50)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "additional_benefits_json_kv", nullable = false, length = 1024)
	public String getAdditionalBenefitsJsonKv() {
		return this.additionalBenefitsJsonKv;
	}

	public void setAdditionalBenefitsJsonKv(String additionalBenefitsJsonKv) {
		this.additionalBenefitsJsonKv = additionalBenefitsJsonKv;
	}

	@Column(name = "plan_name", nullable = false, length = 50)
	public String getPlan() {
		return this.plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	@Column(name = "amount", nullable = false, precision = 53, scale = 0)
	public double getAmount() {
		return this.amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "application_date", nullable = false, length = 23)
	public Date getApplicationDate() {
		return this.applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	@Column(name = "application_status", nullable = false, length = 40)
	public String getApplicationStatus() {
		return this.applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "application", cascade = CascadeType.ALL)
	public Set<BillingInformation> getBillingInformations() {
		return this.billingInformations;
	}

	public void setBillingInformations(Set<BillingInformation> billingInformations) {
		this.billingInformations = billingInformations;
	}

}
