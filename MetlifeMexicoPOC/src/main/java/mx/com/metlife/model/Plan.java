package mx.com.metlife.model;
// Generated 2/06/2018 10:31:41 PM by Hibernate Tools 4.3.5.Final

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * MtfPlan generated by hbm2java
 */
@Entity
@Table(name = "MTF_Plan", schema = "dbo", catalog = "MTF_DB_DE")
public class Plan implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer planId;
	private String planName;
	private String planDescription;
	private Set<PlanBenefitRelated> planBenefitRelateds = new HashSet<PlanBenefitRelated>(0);

	public Plan() {
	}

	public Plan(String planName) {
		this.planName = planName;
	}

	public Plan(String planName, String planDescription, Set<PlanBenefitRelated> mtfPlanBenefitRelateds) {
		this.planName = planName;
		this.planDescription = planDescription;
		this.planBenefitRelateds = mtfPlanBenefitRelateds;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "PK_plan_id", unique = true, nullable = false)
	public Integer getPlanId() {
		return this.planId;
	}

	public void setPlanId(Integer pkPlanId) {
		this.planId = pkPlanId;
	}

	@Column(name = "plan_name", nullable = false, length = 50)
	public String getPlanName() {
		return this.planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	@Column(name = "plan_description", length = 80)
	public String getPlanDescription() {
		return this.planDescription;
	}

	public void setPlanDescription(String planDescription) {
		this.planDescription = planDescription;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "plan")
	public Set<PlanBenefitRelated> getPlanBenefitRelateds() {
		return this.planBenefitRelateds;
	}

	public void setPlanBenefitRelateds(Set<PlanBenefitRelated> mtfPlanBenefitRelateds) {
		this.planBenefitRelateds = mtfPlanBenefitRelateds;
	}
	
	public Plan copyPlanWithouPlanBenefitsRelated() {
		Plan planNew = new Plan();
		planNew.setPlanId(getPlanId());
		planNew.setPlanName(getPlanName());
		planNew.setPlanDescription(getPlanName());
		return planNew;
	}

}
