package mx.com.metlife.model;
// Generated 2/06/2018 10:31:41 PM by Hibernate Tools 4.3.5.Final

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * MtfCustomer generated by hbm2java
 */
@Entity
@Table(name = "MTF_Customer", schema = "dbo", catalog = "MTF_DB_DE")
public class Customer implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer customerId;
	private String lastName;
	private String MLastName;
	private String firstName;
	private String rfc;
	private String birthDate;
	private String height;
	private String weight;
	private String curp;
	private String gender;
	private String maritalStatus;
	private String customerIdType;
	private String customerIdNumber;
	private String countryBirth;
	private String nationalities;
	private String streetName;
	private String streetNumberExternal;
	private String streetNumberInternal;
	private String zipCode;
	private String neighborhood;
	private String municipality;
	private String city;
	private String state;
	private String cty;
	private String cellPhoneNumber;
	private String homePhoneNumber;
	private String email;

	public Customer() {
	}

	public Customer(String lastName, String MLastName, String firstName, String rfc, String birthDate, String height,
			String weight, String curp, String gender, String maritalStatus, String customerIdType,
			String customerIdNumber, String countryBirth, String nationalities, String streetName,
			String streetNumberExternal, String streetNumberInternal, String zipCode, String neighborhood,
			String municipality, String city, String state, String cty, String cellPhoneNumber, String homePhoneNumber,
			String email) {
		this.lastName = lastName;
		this.MLastName = MLastName;
		this.firstName = firstName;
		this.rfc = rfc;
		this.birthDate = birthDate;
		this.height = height;
		this.weight = weight;
		this.curp = curp;
		this.gender = gender;
		this.maritalStatus = maritalStatus;
		this.customerIdType = customerIdType;
		this.customerIdNumber = customerIdNumber;
		this.countryBirth = countryBirth;
		this.nationalities = nationalities;
		this.streetName = streetName;
		this.streetNumberExternal = streetNumberExternal;
		this.streetNumberInternal = streetNumberInternal;
		this.zipCode = zipCode;
		this.neighborhood = neighborhood;
		this.municipality = municipality;
		this.city = city;
		this.state = state;
		this.cty = cty;
		this.cellPhoneNumber = cellPhoneNumber;
		this.homePhoneNumber = homePhoneNumber;
		this.email = email;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "PK_mtp_customer_id", unique = true, nullable = false)
	public Integer getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(Integer pkMtpCustomerId) {
		this.customerId = pkMtpCustomerId;
	}

	@Column(name = "last_name", nullable = false, length = 40)
	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "m_last_name", nullable = false, length = 40)
	public String getMLastName() {
		return this.MLastName;
	}

	public void setMLastName(String MLastName) {
		this.MLastName = MLastName;
	}

	@Column(name = "first_name", nullable = false, length = 40)
	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "rfc", nullable = false, length = 13)
	public String getRfc() {
		return this.rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	@Column(name = "birth_date", nullable = false, length = 40)
	public String getBirthDate() {
		return this.birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	@Column(name = "height", nullable = false, length = 40)
	public String getHeight() {
		return this.height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	@Column(name = "weight", nullable = false, length = 40)
	public String getWeight() {
		return this.weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	@Column(name = "curp", nullable = false, length = 18)
	public String getCurp() {
		return this.curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	@Column(name = "gender", nullable = false, length = 40)
	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Column(name = "marital_status", nullable = false, length = 40)
	public String getMaritalStatus() {
		return this.maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	@Column(name = "customer_id_type", nullable = false, length = 40)
	public String getCustomerIdType() {
		return this.customerIdType;
	}

	public void setCustomerIdType(String customerIdType) {
		this.customerIdType = customerIdType;
	}

	@Column(name = "customer_id_number", nullable = false, length = 40)
	public String getCustomerIdNumber() {
		return this.customerIdNumber;
	}

	public void setCustomerIdNumber(String customerIdNumber) {
		this.customerIdNumber = customerIdNumber;
	}

	@Column(name = "country_birth", nullable = false, length = 40)
	public String getCountryBirth() {
		return this.countryBirth;
	}

	public void setCountryBirth(String countryBirth) {
		this.countryBirth = countryBirth;
	}

	@Column(name = "nationalities", nullable = false, length = 40)
	public String getNationalities() {
		return this.nationalities;
	}

	public void setNationalities(String nationalities) {
		this.nationalities = nationalities;
	}

	@Column(name = "street_name", nullable = false, length = 40)
	public String getStreetName() {
		return this.streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	@Column(name = "street_number_external", nullable = false, length = 40)
	public String getStreetNumberExternal() {
		return this.streetNumberExternal;
	}

	public void setStreetNumberExternal(String streetNumberExternal) {
		this.streetNumberExternal = streetNumberExternal;
	}

	@Column(name = "street_number_internal", nullable = false, length = 40)
	public String getStreetNumberInternal() {
		return this.streetNumberInternal;
	}

	public void setStreetNumberInternal(String streetNumberInternal) {
		this.streetNumberInternal = streetNumberInternal;
	}

	@Column(name = "zip_code", nullable = false, length = 40)
	public String getZipCode() {
		return this.zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@Column(name = "neighborhood", nullable = false, length = 40)
	public String getNeighborhood() {
		return this.neighborhood;
	}

	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}

	@Column(name = "municipality", nullable = false, length = 40)
	public String getMunicipality() {
		return this.municipality;
	}

	public void setMunicipality(String municipality) {
		this.municipality = municipality;
	}

	@Column(name = "city", nullable = false, length = 40)
	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "state", nullable = false, length = 40)
	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "cty", nullable = false, length = 40)
	public String getCty() {
		return this.cty;
	}

	public void setCty(String cty) {
		this.cty = cty;
	}

	@Column(name = "cell_phone_number", nullable = false, length = 40)
	public String getCellPhoneNumber() {
		return this.cellPhoneNumber;
	}

	public void setCellPhoneNumber(String cellPhoneNumber) {
		this.cellPhoneNumber = cellPhoneNumber;
	}

	@Column(name = "home_phone_number", nullable = false, length = 40)
	public String getHomePhoneNumber() {
		return this.homePhoneNumber;
	}

	public void setHomePhoneNumber(String homePhoneNumber) {
		this.homePhoneNumber = homePhoneNumber;
	}

	@Column(name = "email", nullable = false, length = 60)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
