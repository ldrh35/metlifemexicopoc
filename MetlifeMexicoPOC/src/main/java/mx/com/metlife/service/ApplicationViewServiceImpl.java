package mx.com.metlife.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.metlife.dao.ApplicationViewDao;
import mx.com.metlife.model.ApplicationSearchView;

@Service
public class ApplicationViewServiceImpl implements ApplicationViewService {
	
	@Autowired
	private ApplicationViewDao applicationViewDao;
	
	public void setApplicationViewDao(ApplicationViewDao applicationViewDao) {
		this.applicationViewDao = applicationViewDao;
	}

	@Override
	@Transactional
	public List<ApplicationSearchView> getAllApplicationDetail() {
		return applicationViewDao.getAllApplicationDetail();
	}

	@Override
	@Transactional
	public List<ApplicationSearchView> searchApplications(String filter) {
		return applicationViewDao.searchApplications(filter);
	}


}
