package mx.com.metlife.service;

import java.util.List;

import mx.com.metlife.model.ApplicationSearchView;

public interface ApplicationViewService {
	
	public List<ApplicationSearchView> getAllApplicationDetail();
	public List<ApplicationSearchView> searchApplications(String filter);

}
