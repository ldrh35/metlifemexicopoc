package mx.com.metlife.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.metlife.dao.PlanDao;
import mx.com.metlife.model.Plan;

@Service
public class PlanServiceImpl implements PlanService {

	@Autowired
	private PlanDao planDao;
	
	public PlanDao getPlanDao() {
		return planDao;
	}
	
	public void setPlanDao(PlanDao planDao) {
		this.planDao = planDao;
	}

	@Override
	@Transactional
	public List<Plan> getPlans() {
		return planDao.getPlans();
	}

}
