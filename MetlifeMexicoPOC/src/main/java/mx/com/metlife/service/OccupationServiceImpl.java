package mx.com.metlife.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.metlife.dao.OccupationDao;
import mx.com.metlife.model.Occupation;

@Service
public class OccupationServiceImpl implements OccupationService {
	
	@Autowired
	private OccupationDao occupationDao;

	public OccupationDao getOccupationDao() {
		return occupationDao;
	}

	public void setOccupationDao(OccupationDao occupationDao) {
		this.occupationDao = occupationDao;
	}

	@Override
	@Transactional
	public Occupation getById(Integer occupationId) {
		return occupationDao.getById(occupationId);
	}

	@Override
	@Transactional
	public Occupation getByCode(String code) {
		return occupationDao.getByCode(code);
	}

	@Override
	@Transactional
	public List<Occupation> searchOccupationsByCodeOrName(String filter) {
		return occupationDao.searchOccupationsByCodeOrName(filter);
	} 
}
