package mx.com.metlife.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.metlife.dao.ApplicationDao;
import mx.com.metlife.dao.EmployerDao;
import mx.com.metlife.dao.OccupationDao;
import mx.com.metlife.model.Application;
import mx.com.metlife.model.BillingInformation;
import mx.com.metlife.model.Employer;
import mx.com.metlife.model.Occupation;

@Service
public class ApplicationServiceImpl implements ApplicationService {
	
	@Autowired
	private ApplicationDao applicationDao;
	@Autowired
	private EmployerDao employerDao;
	@Autowired
	private OccupationDao occupationDao;

	public void setApplicationDao(ApplicationDao applicationDao) {
		this.applicationDao = applicationDao;
	}

	public EmployerDao getEmployerDao() {
		return employerDao;
	}

	public void setEmployerDao(EmployerDao employerDao) {
		this.employerDao = employerDao;
	}

	public OccupationDao getOccupationDao() {
		return occupationDao;
	}

	public void setOccupationDao(OccupationDao occupationDao) {
		this.occupationDao = occupationDao;
	}
	
	public Application getEmployerAndOccupation(Application application){
		for(BillingInformation billInfo: application.getBillingInformations()){
			Employer tempEmployer = this.employerDao.get(billInfo.getEmployer().getEmployerId());
			if(tempEmployer != null){
				billInfo.setEmployer(tempEmployer);
			}
		}
		Occupation tempOccupation = this.occupationDao.getById(application.getOccupation().getOccupationId());
		if(tempOccupation != null){
			application.setOccupation(tempOccupation);
		}
		return application;
	}

	@Override
	@Transactional
	public Application saveApplication(Application application) {
		getEmployerAndOccupation(application);
		applicationDao.saveApplication(application);
		return application;
	}

	@Override
	@Transactional
	public Application createApplication(Application application) {
		getEmployerAndOccupation(application);
		this.applicationDao.createApplication(application);
		return application;
	}

	@Override
	@Transactional
	public void deleteApplication(Integer id) {
		this.applicationDao.deleteApplication(id);
	}

	@Override
	@Transactional
	public Application get(Integer id) {
		return applicationDao.get(id);
	}
	
	@Override
	@Transactional
	public Application save(Application object) {
		return applicationDao.save(object);
	}

	@Override
	@Transactional
	public boolean exist(Integer id) {
		return applicationDao.exist(id);
	}

	@Override
	@Transactional
	public boolean remove(Application object) {
		return applicationDao.remove(object);
	}

	@Override
	@Transactional
	public Application update(Application object) {
		return applicationDao.update(object);
	}
	
}
