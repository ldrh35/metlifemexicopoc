package mx.com.metlife.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.metlife.dao.PrivacyLogDao;
import mx.com.metlife.model.PrivacyLog;

@Service
public class PrivacyLogServiceImpl implements PrivacyLogService {

	@Autowired
	private PrivacyLogDao privacyLogDao;
		
	public PrivacyLogDao getPrivacyLogDao() {
		return privacyLogDao;
	}

	public void setPrivacyLogDao(PrivacyLogDao privacyLogDao) {
		this.privacyLogDao = privacyLogDao;
	}

	@Override
	@Transactional
	public PrivacyLog addPrivacyLog(PrivacyLog privacyLog) {
		return privacyLogDao.addPrivacyLog(privacyLog);
	}

	@Override
	@Transactional
	public List<PrivacyLog> getPrivacyLogLastMonth() {
		return privacyLogDao.getPrivacyLogLastMonth();
	}

	@Override
	@Transactional
	public List<PrivacyLog> getPrivacyLogByApplicationId(Integer applicationId) {
		return privacyLogDao.getPrivacyLogByApplicationId(applicationId);
	}

	@Override
	@Transactional
	public List<PrivacyLog> getPrivacyLogByCustomerId(Integer customerId) {
		return privacyLogDao.getPrivacyLogByCustomerId(customerId);
	}

}
