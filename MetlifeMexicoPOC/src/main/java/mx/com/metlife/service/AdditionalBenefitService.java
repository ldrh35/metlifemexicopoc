package mx.com.metlife.service;

import java.util.List;

import mx.com.metlife.model.AdditionalBenefit;

public interface AdditionalBenefitService {
	
	public AdditionalBenefit addAdditionalBenefit(AdditionalBenefit additionalBen);//C
	public List<AdditionalBenefit> getAdditionalBenefits();//R
	public AdditionalBenefit updateAdditionalBenefit(AdditionalBenefit additionalBen); //U
	public void deleteAdditionalBenefit(AdditionalBenefit additionalBen); //D

}
