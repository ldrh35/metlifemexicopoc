package mx.com.metlife.service;

import java.util.List;

import mx.com.metlife.model.Plan;

public interface PlanService {
	
	public List<Plan> getPlans();

}
