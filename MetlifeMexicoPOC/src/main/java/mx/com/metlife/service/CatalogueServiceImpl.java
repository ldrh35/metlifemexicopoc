package mx.com.metlife.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.metlife.dao.CatalogueDao;
import mx.com.metlife.model.Catalogue;

@Service
public class CatalogueServiceImpl implements CatalogueService {
	
	@Autowired
	private CatalogueDao catalogueDao;
	
	public CatalogueDao getCatalogueDao() {
		return catalogueDao;
	}

	public void setCatalogueDao(CatalogueDao catalogueDao) {
		this.catalogueDao = catalogueDao;
	}

	@Override
	@Transactional
	public Catalogue getById(Integer catalogueId) {
		return catalogueDao.getById(catalogueId);
	}

	@Override
	@Transactional
	public List<Catalogue> getCataloguesByParentId(Integer parentId) {
		return catalogueDao.getCataloguesByParentId(parentId);
	}
	
	@Override
	@Transactional
	public List<Catalogue> getAllCatalogues() {
		return catalogueDao.getAllCatalogues();
	}
	
	@Override
	@Transactional
	public List<Catalogue> getCataloguesByParentIdList(List<Integer> parentIdList) {
		return catalogueDao.getCataloguesByParentIdList(parentIdList);
	}

}
