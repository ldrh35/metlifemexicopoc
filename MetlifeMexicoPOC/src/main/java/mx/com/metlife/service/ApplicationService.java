package mx.com.metlife.service;

import mx.com.metlife.model.Application;

public interface ApplicationService extends GenericService<Application, Integer> {
	
	public Application saveApplication(Application application);
	public Application createApplication(Application application);
	public void deleteApplication(Integer id);

}
