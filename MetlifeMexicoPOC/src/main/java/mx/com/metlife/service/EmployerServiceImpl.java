package mx.com.metlife.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.metlife.dao.EmployerDao;
import mx.com.metlife.model.Employer;

@Service
public class EmployerServiceImpl implements EmployerService {
	
	@Autowired
	private EmployerDao employerDao;
	
	public void setEmployerDAO(EmployerDao employerDao) {
		this.employerDao = employerDao;
	}
	
	@Override
	@Transactional
	public List<Employer> search(String filter) {
		return employerDao.search(filter);
	}
	

}
