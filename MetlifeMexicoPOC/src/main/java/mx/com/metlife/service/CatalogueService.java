package mx.com.metlife.service;

import java.util.List;

import mx.com.metlife.model.Catalogue;

public interface CatalogueService {
	
	public Catalogue getById(Integer catalogueId);
	public List<Catalogue> getCataloguesByParentId(Integer parentId);
	public List<Catalogue> getAllCatalogues();
	public List<Catalogue> getCataloguesByParentIdList(List<Integer> parentIdList);

}
