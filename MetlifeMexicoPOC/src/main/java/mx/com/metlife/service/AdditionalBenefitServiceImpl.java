package mx.com.metlife.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.metlife.dao.AdditionalBenefitDao;
import mx.com.metlife.model.AdditionalBenefit;

@Service
public class AdditionalBenefitServiceImpl implements AdditionalBenefitService {
	

	@Autowired
	private AdditionalBenefitDao additionalBenefitDao;
	
	public AdditionalBenefitDao getAdditionalBenefitDao() {
		return additionalBenefitDao;
	}

	public void setAdditionalBenefitDao(AdditionalBenefitDao additionalBenefitDao) {
		this.additionalBenefitDao = additionalBenefitDao;
	}

	@Override
	@Transactional
	public AdditionalBenefit addAdditionalBenefit(AdditionalBenefit additionalBen) {
		return this.additionalBenefitDao.addAdditionalBenefit(additionalBen);
	}

	@Override
	@Transactional
	public List<AdditionalBenefit> getAdditionalBenefits() {
		return this.additionalBenefitDao.getAdditionalBenefits();
	}

	@Override
	@Transactional
	public AdditionalBenefit updateAdditionalBenefit(AdditionalBenefit additionalBen) {
		return this.additionalBenefitDao.updateAdditionalBenefit(additionalBen);
	}

	@Override
	@Transactional
	public void deleteAdditionalBenefit(AdditionalBenefit additionalBen) {
		this.additionalBenefitDao.deleteAdditionalBenefit(additionalBen);
	}

}
