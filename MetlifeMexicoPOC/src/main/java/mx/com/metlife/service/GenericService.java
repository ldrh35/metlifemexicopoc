package mx.com.metlife.service;

public interface GenericService<T, PK> {
	
	public T get(PK id);
	
	public T save (T object);
	
	public boolean exist(PK id);
	
	public boolean remove(T object);
	
	public T update(T object);

}
