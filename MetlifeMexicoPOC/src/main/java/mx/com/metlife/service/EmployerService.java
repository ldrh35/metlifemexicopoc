package mx.com.metlife.service;

import java.util.List;

import mx.com.metlife.model.Employer;

public interface EmployerService {
	
	public List<Employer> search(String query);

}
