package mx.com.metlife.service;

import java.util.List;

import mx.com.metlife.model.PrivacyLog;

public interface PrivacyLogService {
	
	public PrivacyLog addPrivacyLog(PrivacyLog privacyLog);
	public List<PrivacyLog> getPrivacyLogLastMonth();
	public List<PrivacyLog> getPrivacyLogByApplicationId(Integer applicationId);
	public List<PrivacyLog> getPrivacyLogByCustomerId(Integer customerId);

}
