package mx.com.metlife.service;

import java.util.List;

import mx.com.metlife.model.Occupation;

public interface OccupationService {
	
	public Occupation getById(Integer occupationId);
	public Occupation getByCode(String code);
	public List<Occupation> searchOccupationsByCodeOrName(String filter);

}
