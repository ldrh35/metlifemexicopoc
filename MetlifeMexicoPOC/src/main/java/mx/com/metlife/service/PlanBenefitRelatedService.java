package mx.com.metlife.service;

import java.util.List;

import mx.com.metlife.model.PlanBenefitRelated;

public interface PlanBenefitRelatedService {
	
	public List<PlanBenefitRelated> getPlanBenefitRelated();
	public PlanBenefitRelated createPlanBenefitRelated(PlanBenefitRelated planBenefitRelated);

}
