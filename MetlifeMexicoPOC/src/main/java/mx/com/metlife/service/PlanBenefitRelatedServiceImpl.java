package mx.com.metlife.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.metlife.dao.PlanBenefitRelatedDao;
import mx.com.metlife.model.PlanBenefitRelated;

@Service
public class PlanBenefitRelatedServiceImpl implements PlanBenefitRelatedService {
	
	@Autowired
	private PlanBenefitRelatedDao planBenefitRelatedDao;

	public PlanBenefitRelatedDao getPlanBenefitRelatedDao() {
		return planBenefitRelatedDao;
	}

	public void setPlanBenefitRelatedDao(PlanBenefitRelatedDao planBenefitRelatedDao) {
		this.planBenefitRelatedDao = planBenefitRelatedDao;
	}

	@Override
	@Transactional
	public List<PlanBenefitRelated> getPlanBenefitRelated() {
		return this.planBenefitRelatedDao.getPlanBenefitRelated();
	}

	@Override
	@Transactional
	public PlanBenefitRelated createPlanBenefitRelated(PlanBenefitRelated planBenefitRelated) {
		return this.planBenefitRelatedDao.createPlanBenefitRelated(planBenefitRelated);
	}

}
