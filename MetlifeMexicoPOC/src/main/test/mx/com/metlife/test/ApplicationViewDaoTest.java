package mx.com.metlife.test;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import mx.com.metlife.dao.ApplicationViewDaoImpl;
import mx.com.metlife.model.ApplicationSearchView;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:hibernate.cfg.xml", "file:src/main/java/hibernate.cfg.xml", "file:**/spring-servlet.xml"})
@Transactional
public class ApplicationViewDaoTest {
	private static final String FILTER = "6";
	private ApplicationViewDaoImpl applicationViewDaoImpl;
	
	@Autowired
	private SessionFactory sessionFactory;
		
		public void setSessionFactory(SessionFactory sf){
			this.sessionFactory = sf;
		}
		
		public Session getSession(){
			return this.sessionFactory.getCurrentSession();
		}

		@Before
		public void setUp(){
			applicationViewDaoImpl = new ApplicationViewDaoImpl();
			applicationViewDaoImpl.setSessionFactory(sessionFactory);
		}
	
		@Test
		public void getAllApplicationDetail() {
			System.out.println("*** Start test getAllApplicationDetail ***");
			
			List<ApplicationSearchView> results = applicationViewDaoImpl.getAllApplicationDetail();
			System.out.println(results);
			
			Assert.assertNotNull("Application View List must not be null.", results);
			Assert.assertFalse("Application View List must not be empty.", results.isEmpty());
			
			System.out.println("*** End test getAllApplicationDetail ***");
		}

		@Test
		public void searchApplications() {
			System.out.println("*** Start test searchApplications using " + FILTER + " as Filter. ***");
			
			List<ApplicationSearchView> results = applicationViewDaoImpl.searchApplications(FILTER);
			System.out.println(results);
			
			Assert.assertNotNull("Application View List must not be null.", results);
			Assert.assertFalse("Application View List must not be empty.", results.isEmpty());
			System.out.println("*** End test searchApplications ***");
		}
}
