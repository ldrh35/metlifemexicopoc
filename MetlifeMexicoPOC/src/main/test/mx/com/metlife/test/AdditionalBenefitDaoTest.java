package mx.com.metlife.test;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import mx.com.metlife.dao.AdditionalBenefitDaoImpl;
import mx.com.metlife.model.AdditionalBenefit;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:hibernate.cfg.xml", "file:src/main/java/hibernate.cfg.xml", "file:**/spring-servlet.xml"})
@Transactional
@FixMethodOrder(org.junit.runners.MethodSorters.JVM)
public class AdditionalBenefitDaoTest {
	private AdditionalBenefitDaoImpl additionalBenefitDaoImpl;
	private AdditionalBenefit additionalBenefit;
	
	@Autowired
	private SessionFactory sessionFactory;
		
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
		
	public Session getSession(){
		return this.sessionFactory.getCurrentSession();
	}

	@Before
	public void setUp(){
		additionalBenefitDaoImpl = new AdditionalBenefitDaoImpl();
		additionalBenefitDaoImpl.setSessionFactory(sessionFactory);
		//create test object
		additionalBenefit = new AdditionalBenefit();
		additionalBenefit.setBenefitName("Test Benefit");
		additionalBenefit.setBenefitCode("TEST");
		additionalBenefit.setBenefitDescription("This is a row from JUnit Test");
	}
	
	@Test
	public void addUpdateDeleteAdditionalBenefit() {
		System.out.println("*** Start test addAdditionalBenefit ***");
		
		additionalBenefit = additionalBenefitDaoImpl.addAdditionalBenefit(additionalBenefit);

		if(additionalBenefit != null){
			System.out.println("PK_MTF_additional_benefit_id: " + additionalBenefit.getAdditionalBenefitId());
		}
		
		Assert.assertNotNull("AdditionalBenefit object must not be null.", additionalBenefit);
		Assert.assertNotNull("AdditionalBenefit object must not be null.", additionalBenefit.getAdditionalBenefitId());
		
		System.out.println("*** End test addAdditionalBenefit ***");
		
		updateAdditionalBenefit();
		deleteAdditionalBenefit();
	}
	
	@Test
	public void getAdditionalBenefits() {
		System.out.println("*** Start test getAdditionalBenefits ***");
		List<AdditionalBenefit> results = additionalBenefitDaoImpl.getAdditionalBenefits();
		System.out.println(results);
		
		Assert.assertNotNull("AdditionalBenefit List must not be null.", results);
		Assert.assertFalse("AdditionalBenefit List must not be empty.", results.isEmpty());
		
		System.out.println("*** End test getAdditionalBenefits ***");
	}

	public void updateAdditionalBenefit() {
		System.out.println("*** Start test updateAdditionalBenefit using " + additionalBenefit.getAdditionalBenefitId() + "***");
		additionalBenefit.setBenefitName("Update Test");
		additionalBenefit = additionalBenefitDaoImpl.updateAdditionalBenefit(additionalBenefit);
				
		if(additionalBenefit != null){
			System.out.println("Benefit Name: " + additionalBenefit.getBenefitName());
		}
				
		Assert.assertNotNull("AdditionalBenefit object must not be null.", additionalBenefit);
		Assert.assertEquals(additionalBenefit.getBenefitName(), "Update Test");
				
		System.out.println("*** End test updateAdditionalBenefit ***");
	}

	public void deleteAdditionalBenefit() {
		System.out.println("*** Start Test deleteAdditionalBenefit ***");
		additionalBenefitDaoImpl.deleteAdditionalBenefit(additionalBenefit);
		boolean found = false;
		for(AdditionalBenefit ab : additionalBenefitDaoImpl.getAdditionalBenefits()){
			if(ab.getAdditionalBenefitId().intValue() == additionalBenefit.getAdditionalBenefitId().intValue()){
				found = true;
			}
		}
		
		Assert.assertFalse("The object was not deleted.", found);
		System.out.println("*** End Test deleteAdditionalBenefit ***");
	}
}
