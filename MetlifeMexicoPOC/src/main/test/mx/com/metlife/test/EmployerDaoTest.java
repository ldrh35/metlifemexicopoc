package mx.com.metlife.test;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import mx.com.metlife.dao.EmployerDao;
import mx.com.metlife.model.Catalogue;
import mx.com.metlife.model.Employer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:hibernate.cfg.xml", "file:src/main/java/hibernate.cfg.xml", "file:**/spring-servlet.xml"})
@Transactional
public class EmployerDaoTest {
	
	private static final int CATALOGUEID = 3;
	private static final int PARENTID = 1;
	
	@Autowired
	private SessionFactory sessionFactory;
		
	public Session getSession(){
		return this.sessionFactory.getCurrentSession();
	}
	
	@Autowired
	private EmployerDao employerDao;

	public void setEmployerDao(EmployerDao employerDao) {
		this.employerDao = employerDao;
	}

	@Test
	public void getById() {
		System.out.println("*** Start test getById using " + CATALOGUEID + " as ID. ***");
		Employer emp = new Employer();
		emp.setEmployerName("MyEmployer");
		emp.setEmployerDescription("EmployerDescription");
		emp.setEmployerRfc("RUHL871110A23");
		emp = employerDao.save(emp);
		
		Assert.assertNotNull(emp);
		
		Assert.assertNotNull("Catalogue object must not be null.", emp);
		Assert.assertNotNull(emp.getEmployerId());
		System.out.println("*** End test getById ***");
	}
	
	@Test
	public void getCataloguesByParentId() {
		System.out.println("*** Start test getCataloguesByParentId using " + PARENTID + " as PARENTID. ***");
		
		@SuppressWarnings("unchecked")
		List<Catalogue> results = getSession().createCriteria(Catalogue.class).add(Restrictions.eq("parentId", PARENTID)).addOrder(Order.asc("catalogueOrder")).list();
		System.out.println(results);
		
		Assert.assertNotNull("Catalogue List must not be null.", results);
		Assert.assertFalse("Catalogue List must not be empty.", results.isEmpty());
		System.out.println("*** End test getCataloguesByParentId ***");
	}
	
	@Test
	public void getAllCatalogues() {
		System.out.println("*** Start test getAllCatalogues ***");
		
		@SuppressWarnings("unchecked")
		List<Catalogue> results = getSession().createCriteria(Catalogue.class).list();
		System.out.println(results);
		
		Assert.assertNotNull("Catalogue List must not be null.", results);
		Assert.assertFalse("Catalogue List must not be empty.", results.isEmpty());
		System.out.println("*** End test getAllCatalogues ***");
	}	    
}
