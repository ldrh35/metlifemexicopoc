package mx.com.metlife.test;

import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import mx.com.metlife.dao.PrivacyLogDaoImpl;
import mx.com.metlife.model.PrivacyLog;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:hibernate.cfg.xml", "file:src/main/java/hibernate.cfg.xml", "file:**/spring-servlet.xml"})
@Transactional
public class PrivacyLogDaoTest {
	private static final int APPLICATIONID = 1;
	private static final Integer CUSTOMERID = 2;
	private PrivacyLogDaoImpl privacyLogDaoImpl;
	private PrivacyLog privacyLog;
	
	@Autowired
	private SessionFactory sessionFactory;
		
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	public Session getSession(){
		return this.sessionFactory.getCurrentSession();
	}
	
	@Before
	public void setUp(){
		privacyLogDaoImpl = new PrivacyLogDaoImpl();
		privacyLogDaoImpl.setSessionFactory(sessionFactory);
		privacyLog = getPrivacyLogTest();
	}
	
	private PrivacyLog getPrivacyLogTest(){
		PrivacyLog pl = new PrivacyLog();
		pl.setApplicationId(6);
		pl.setCustomerId(2);
		pl.setEventDate(new Date());
		pl.setUserName("user_test");
		pl.setInformationDataConsulted("JUnit Test");
		return pl;
	}
	
	@Test
	public void addPrivacyLog() {
		System.out.println("*** Start test addPrivacyLog ***");
		
		privacyLog = privacyLogDaoImpl.addPrivacyLog(privacyLog);
		if(privacyLog != null){
			System.out.println("privacyLog: " + privacyLog.getPrivacyLogId());
		}
		
		Assert.assertNotNull("privacyLog object must not be null.", privacyLog);
		Assert.assertNotNull("privacyLog ID must not be null.", privacyLog.getPrivacyLogId());
				
		System.out.println("*** End test addPrivacyLog ***");
	}

	@Test
	public void getPrivacyLogLastMonth() {
		System.out.println("*** Start test getPrivacyLogLastMonth ***");
		
		List<PrivacyLog> results = privacyLogDaoImpl.getPrivacyLogLastMonth();
		System.out.println(results);
		
		Assert.assertNotNull("privacyLog List must not be null.", results);
		Assert.assertFalse("privacyLog List must not be empty.", results.isEmpty());
		System.out.println("*** End test getPrivacyLogLastMonth ***");
	}

	@Test
	public void getPrivacyLogByApplicationId() {
		System.out.println("*** Start test getPrivacyLogByApplicationId ***");
		
		List<PrivacyLog> results = privacyLogDaoImpl.getPrivacyLogByApplicationId(APPLICATIONID);
		System.out.println(results);
		
		Assert.assertNotNull("privacyLog List must not be null.", results);
		Assert.assertFalse("privacyLog List must not be empty.", results.isEmpty());
		System.out.println("*** End test getPrivacyLogByApplicationId ***");
	}

	@Test
	public void getPrivacyLogByCustomerId() {
		System.out.println("*** Start test getPrivacyLogByCustomerId ***");
		
		List<PrivacyLog> results = privacyLogDaoImpl.getPrivacyLogByCustomerId(CUSTOMERID);
		System.out.println(results);
		
		Assert.assertNotNull("privacyLog List must not be null.", results);
		Assert.assertFalse("privacyLog List must not be empty.", results.isEmpty());
		System.out.println("*** End test getPrivacyLogByCustomerId ***");
	}
}
