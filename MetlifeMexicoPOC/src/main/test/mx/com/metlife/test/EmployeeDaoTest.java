package mx.com.metlife.test;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import mx.com.metlife.dao.EmployerDaoImpl;
import mx.com.metlife.model.Employer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:hibernate.cfg.xml", "file:src/main/java/hibernate.cfg.xml", "file:**/spring-servlet.xml"})
@Transactional
public class EmployeeDaoTest {
	private static final String FILTER = "MetLife Mexico S. A.";
	private EmployerDaoImpl employerDaoImpl;
	
	@Autowired
	private SessionFactory sessionFactory;
		
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	public Session getSession(){
		return this.sessionFactory.getCurrentSession();
	}

	@Before
	public void setUp(){
		employerDaoImpl = new EmployerDaoImpl();
		employerDaoImpl.setSessionFactory(sessionFactory);
	}
	
	@Test
	public void search() {
		System.out.println("*** Start test search ***");
		
		List<Employer> results = employerDaoImpl.search(FILTER);
		
		System.out.println("Results: " + results); 
		Assert.assertNotNull("Employer List must not be null.", results);
		Assert.assertFalse("Employer List must not be empty.", results.isEmpty());
		
		for(Employer employer : results){
			System.out.println(employer.getEmployerName());
		}
		
		System.out.println("*** End test search ***");
	}
}
