package mx.com.metlife.test;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import mx.com.metlife.dao.OccupationDaoImpl;
import mx.com.metlife.model.Occupation;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:hibernate.cfg.xml", "file:src/main/java/hibernate.cfg.xml", "file:**/spring-servlet.xml"})
@Transactional
public class OccupationDaoTest {
	private static final int OCCUPATIONID = 1;
	private static final String OCCUPATION_NAME = "Enginer";
	private static final String CODE = "001";
	private static final String FILTER = "test";
	private OccupationDaoImpl occupationDaoImpl;
	
	@Autowired
	private SessionFactory sessionFactory;
		
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	public Session getSession(){
		return this.sessionFactory.getCurrentSession();
	}

	@Before
	public void setUp(){
		occupationDaoImpl = new OccupationDaoImpl();
		occupationDaoImpl.setSessionFactory(sessionFactory);
	}
	
	@Test
	public void getById() {
		System.out.println("*** Start test getById using " + OCCUPATIONID + " as OccupationId. ***");
		
		Occupation occupation = occupationDaoImpl.getById(OCCUPATIONID);
		Assert.assertNotNull(occupation);
		Assert.assertEquals(OCCUPATION_NAME, occupation.getOcupationName());
		System.out.println("Result: " + occupation.getOcupationName());

		System.out.println("*** End test getById ***");
	}

	@Test
	public void getByCode() {
		System.out.println("*** Start test getByCode using " + CODE + " as Code. ***");
		
		Occupation occupation = occupationDaoImpl.getByCode(CODE);
		Assert.assertNotNull(occupation);
		Assert.assertEquals(OCCUPATION_NAME, occupation.getOcupationName());
		System.out.println("Result: " + occupation.getOcupationName());
		
		System.out.println("*** End test getByCode ***");
	}

	@Test
	public void searchOccupationsByCodeOrName() {
		System.out.println("*** Start test searchOccupationsByCodeOrName using " + FILTER + " as Code or Name. ***");
		
		List<Occupation> results = occupationDaoImpl.searchOccupationsByCodeOrName(OCCUPATION_NAME);
			
		Assert.assertNotNull("Occupation List must not be null.", results);
		Assert.assertFalse("Occupation List must not be empty.", results.isEmpty());
		
		System.out.println("Results: "); 
		for(Occupation occupation : results){
			System.out.println(occupation.getOcupationName());
		}
		
		System.out.println("*** End test searchOccupationsByCodeOrName ***");
	}
	
	
}