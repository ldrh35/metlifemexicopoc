package mx.com.metlife.test;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import mx.com.metlife.dao.PlanBenefitRelatedDaoImpl;
import mx.com.metlife.model.PlanBenefitRelated;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:hibernate.cfg.xml", "file:src/main/java/hibernate.cfg.xml", "file:**/spring-servlet.xml"})
@Transactional
public class PlanBenefitRelatedDaoTest {
	private PlanBenefitRelatedDaoImpl planDaoImpl;
	
	@Autowired
	private SessionFactory sessionFactory;
		
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	public Session getSession(){
		return this.sessionFactory.getCurrentSession();
	}
	
	@Before
	public void setUp(){
		planDaoImpl = new PlanBenefitRelatedDaoImpl();
		planDaoImpl.setSessionFactory(sessionFactory);
	}
	
	@Test
	public void getPlanBenefitRelated() {
		System.out.println("*** Start test getPlanBenefitRelated ***");
		
		List<PlanBenefitRelated> results = planDaoImpl.getPlanBenefitRelated();
		
		System.out.println("Results: " + results); 
		Assert.assertNotNull("PlanBenefitRelated List must not be null.", results);
		Assert.assertFalse("PlanBenefitRelated List must not be empty.", results.isEmpty());
		
		for(PlanBenefitRelated planBenefitRelated : results){
			System.out.println(planBenefitRelated.getPlan().getPlanName());
		}
		
		System.out.println("*** End test getPlanBenefitRelated ***");
	}
}
