package mx.com.metlife.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import mx.com.metlife.dao.ApplicationDao;
import mx.com.metlife.model.Application;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"file:hibernate.cfg.xml", "file:src/main/java/hibernate.cfg.xml", "file:**/spring-servlet.xml"})
@Transactional
public class ApplicationDaoTest {
	
	@Autowired
	private SessionFactory sessionFactory;
		
	public Session getSession(){
		return this.sessionFactory.getCurrentSession();
	}
	
	@Autowired
	private ApplicationDao applicationDao;

	public void setApplicationDao(ApplicationDao applicationDao) {
		this.applicationDao = applicationDao;
	}

	@Test
	public void getById() {
		Application application = new Application();
		application = applicationDao.get(6);
		Assert.assertNotNull(application);
		Assert.assertNotNull("Catalogue object must not be null.", application);
		Assert.assertNotNull(application.getApplicationId());
		System.out.println("*** End test getById ***");
	}
	
	@Test
	public void save() {
	}
}
